#ifndef __DIGO_SCHEDULE_H__
#define __DIGO_SCHEDULE_H__

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "cJSON.h"

#include <esp_log.h>

typedef struct schedule_target_t schedule_target_t;
typedef struct schedule_t schedule_t;

struct schedule_target_t {
    int power;
    int temp;  
    schedule_target_t* next;
};

typedef struct schedule_p {
} schedule_p;

struct schedule_t {
    const schedule_p* proc;
    char* name;
    bool enable;
    bool notify;
    bool execute;
    uint32_t time;
    uint32_t repeat;
    schedule_t* next;
    schedule_target_t* target;
};

schedule_t* SCHED_Load(void);
void SCHED_Dispose(schedule_t* object);
int SCHED_Add(schedule_t* object, cJSON* schedule);
int SCHED_Edit(schedule_t* object, cJSON* schedule);
int SCHED_Remove(schedule_t* object, cJSON* schedule);
int SCHED_Export(schedule_t* object, char* buffer, int size);
void SCHED_Process(schedule_t* object, uint32_t timestamp);
int SCHED_GetNumber(void);
void SCHED_RegisterSchedCallback(void(*callback)(char* sname, char* name, int value));
void SCHED_RegisterSchedTickCallback(void(*callback)(char* sname, char* name, int value, int hour, int min));

int SCHED_DeleteAll(void);
int SCHED_EnableAll(void);
int SCHED_DisableAll(void);
int SCHED_Disable(char* sname, uint8_t minute);
void SCHED_SaveToFlash(void);

#endif/*__DIGO_SCHEDULE_H__*/
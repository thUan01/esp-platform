#include "digo_syslog.h"

#include <nvs_flash.h>
#include <time.h>

#include <esp_partition.h>
#include <inttypes.h>

#include "digo_time_service.h"

typedef struct {
    const esp_partition_t *pPartition;
    uint32_t u32OffsetAddress; 
    uint32_t u32BlockIndex;
}syslog_t;

syslog_t syslog;

static const char *TAG = "SYSTEM_LOG";

#if (CONFIG_LOG_SYSLOG_ENABLED == true)

#define SYSLOG_LOGI(tag,fmt, ...)           ESP_LOGI(tag,fmt, ##__VA_ARGS__)
#define SYSLOG_LOGE(tag,fmt, ...)           ESP_LOGE(tag,fmt, ##__VA_ARGS__)
#define SYSLOG_LOGW(tag,fmt, ...)           ESP_LOGW(tag,fmt, ##__VA_ARGS__)

#elif (CONFIG_LOG_SYSLOG_ENABLED == false)

#define SYSLOG_LOGI(tag,fmt, ...)  
#define SYSLOG_LOGE(tag,fmt, ...)  
#define SYSLOG_LOGW(tag,fmt, ...)  

#endif /*CONFIG_LOG_SYSLOG_ENABLED*/


static const esp_partition_t *SYSLOG_FindPartition(void)
{
    esp_err_t err = nvs_flash_init();
    ESP_ERROR_CHECK(err);

    esp_partition_iterator_t esp_partition_it = esp_partition_find(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_NVS, CONFIG_SYSLOG_PARTITION_NAME);
    if (esp_partition_it == NULL)
    {
        return NULL;
    }
    const esp_partition_t *partition = esp_partition_get(esp_partition_it);
    esp_partition_iterator_release(esp_partition_it);
    return partition;
}


static int32_t SYSLOG_FindOffsetAddress(const esp_partition_t *partition)
{
    uint32_t timestamp = 0;
    uint8_t recordBuf[8] = {0};
    uint32_t offset = 0;
    memset(recordBuf, 0xff, 8);

    while (timestamp != 0xffffffff)
    {
        esp_partition_read(partition, offset, recordBuf, 8);
        timestamp = (recordBuf[4] << 24) | (recordBuf[5] << 16) | (recordBuf[6] << 8) | recordBuf[7];
        if (offset + 8 > partition->size)
        {
            break;
        }
        offset += 8;
    }
    return offset - 8;
}


SYSLOG_Error_t SYSLOG_SaveOneRecord(uint8_t *data)
{
    if (syslog.pPartition == NULL)
    {
        SYSLOG_LOGE(TAG, "No SYSLOG Partition Can Be Found");
        return SYSLOG_FAIL;
    }

    if (syslog.u32OffsetAddress % 4096 == 0)
    {
        SYSLOG_LOGI(TAG, "Erase One Page of Syslog Partition [4096 Bytes]");
        esp_partition_erase_range(syslog.pPartition, syslog.u32OffsetAddress, 4096);
    }
    
    esp_err_t err = esp_partition_write(syslog.pPartition, syslog.u32OffsetAddress, data, 8);
    if (err != ESP_OK)
    {
        SYSLOG_LOGE(TAG, "Write Data Fail, rsn = %s, %s(%d)", esp_err_to_name(err), __FUNCTION__, __LINE__);
        return SYSLOG_FAIL;
    }

    if (syslog.u32OffsetAddress > syslog.pPartition->size)
    {
        syslog.u32OffsetAddress = 0;
        SYSLOG_LOGW(TAG, "Overflow SYSLOG Partition ----> Reset SYSLOG Partition");
    }
    else
    {
        syslog.u32OffsetAddress += 8;
    }

    syslog.u32BlockIndex = syslog.u32OffsetAddress / 128;
    return SYSLOG_OK;
    
}


SYSLOG_Error_t SYSLOG_ReadBlockIndex(uint32_t blockIndex, uint8_t *data)
{
    if (syslog.pPartition == NULL)
        return SYSLOG_FAIL;
    SYSLOG_LOGI(TAG, "Reading Block ID = %"PRIu32".......", blockIndex);
    esp_err_t err = esp_partition_read(syslog.pPartition, blockIndex * 128, data, 128);
    if (err != ESP_OK)
    {
        SYSLOG_LOGE(TAG, "Read Block %"PRIu32" Fail, rsn = %s, %s(%d)", blockIndex, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return SYSLOG_FAIL;
    }
    return SYSLOG_OK;
}


SYSLOG_Error_t SYSLOG_Init(void)
{
    syslog.pPartition = SYSLOG_FindPartition();
    if (syslog.pPartition == NULL)
    {
        SYSLOG_LOGE(TAG, "No Syslog Partition Can Be Found");
        return SYSLOG_FAIL;
    }
    else
    {
        SYSLOG_LOGI(TAG, "Partition Found: %s ,0x%"PRIX32" ,0x%"PRIX32"  ", syslog.pPartition->label, syslog.pPartition->address, syslog.pPartition->size);
        syslog.u32OffsetAddress = SYSLOG_FindOffsetAddress(syslog.pPartition);
        syslog.u32BlockIndex = syslog.u32OffsetAddress / 128;
        SYSLOG_LOGI(TAG, "Location To Start Saving SYSLOG Data: Offset %"PRIu32", Block index %"PRIu32"", syslog.u32OffsetAddress, syslog.u32BlockIndex);
    }
    return SYSLOG_OK;
}


uint32_t SYSLOG_GetBlockIndex(void)
{
    return syslog.u32BlockIndex;
}


SYSLOG_Error_t SYSLOG_Save(uint8_t cause, uint16_t value, uint8_t extra)
{
    uint8_t buffer[8] = {0};
    buffer[0] = cause;
    buffer[1] = value >> 8;
    buffer[2] = value & 0xFF;
    buffer[3] = extra;
    uint32_t timestamp = (uint32_t)TIME_GetTimeStamp();
    *(uint32_t *)&buffer[4] = timestamp;
    SYSLOG_LOGI(TAG, "[CAUSE:%"PRIu8"][VALUE:0x%"PRIX16"][EXTRA:%"PRIu8"][TS:%"PRIu32"]", cause, value, extra, timestamp);
    return SYSLOG_SaveOneRecord(buffer);
}

#ifndef __DIGO_WIFI_CONFIG_H__
#define __DIGO_WIFI_CONFIG_H__

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <esp_log.h>

typedef enum {
    WFCFG_OK,
    WFCFG_FAIL,
}WFCFG_Error_t;

typedef enum{
    WFCFG_EVT_WIFI_READY = 0,           /**< ESP32 WiFi ready */
    WFCFG_EVT_SCAN_DONE,                /**< ESP32 finish scanning AP */
    WFCFG_EVT_STA_START,                /**< ESP32 station start */
    WFCFG_EVT_STA_STOP,                 /**< ESP32 station stop */
    WFCFG_EVT_STA_CONNECTED,            /**< ESP32 station connected to AP */
    WFCFG_EVT_STA_DISCONNECTED,         /**< ESP32 station disconnected from AP */
    WFCFG_EVT_STA_AUTHMODE_CHANGE,      /**< the auth mode of AP connected by ESP32 station changed */
    WFCFG_EVT_AP_START,                 /**< ESP32 soft-AP start */
    WFCFG_EVT_AP_STOP,                  /**< ESP32 soft-AP stop */
    WFCFG_EVT_AP_STACONNECTED,          /**< a station connected to ESP32 soft-AP */
    WFCFG_EVT_AP_STADISCONNECTED,       /**< a station disconnected from ESP32 soft-AP */
    WFCFG_EVT_AP_PROBEREQRECVED,        /**< Receive probe request packet in soft-AP interface */
    WFCFG_EVT_STA_GOT_IPv4,               /*!< station got IP from connected AP */
    WFCFG_EVT_STA_GOT_IPv6,               /*!< station got IP from connected AP */
    WFCFG_EVT_AP_STAIPASSIGNED,         /*!< soft-AP assign an IP to a connected station */
    WFCFG_SC_EVT_SCAN_DONE,                /*!< ESP32 station smartconfig has finished to scan for APs */
    WFCFG_SC_EVT_SCAN_DONE_TIMEOUT,
    WFCFG_SC_EVT_FOUND_CHANNEL,            /*!< ESP32 station smartconfig has found the channel of the target AP */
    WFCFG_SC_EVT_GOT_SSID_PSWD,            /*!< ESP32 station smartconfig got the SSID and password */
    WFCFG_SC_EVT_SEND_ACK_DONE,            /*!< ESP32 station smartconfig has sent ACK to cellphone */
    WFCFG_RUN_MODE,
    WFCFG_CONFIG_MODE,
}WFCFG_CallbackEvent_t;

typedef union {
    struct WFCFG_GotIPv4{
        char* ip;
    }ipv4;

    struct WFCFG_GotIPv6{
        char* ip;
    }ipv6;

    struct WFCFG_SC_GotSSIDPWD
    {
        char* ssid;
        char* pwd;
    }scAPInfo;

    struct WFCFG_RunMode
    {
        char* ssid;
        char* pwd;
    }savedAPInfo;
}WFCFG_CallbackParam_t;

typedef void(*WFCFG_Callback)(WFCFG_CallbackEvent_t event_id, WFCFG_CallbackParam_t* param);

void WFCFG_Init(const char* hostName, uint8_t reconnectRetry);

void WFCFG_NetIFInit(void);

void WFCFG_DeInit(void);

bool WFCFG_CheckAPInfoOnNVS(char* ssid, char* password);

uint8_t WFCFG_GetSSID(char* ssid);

uint8_t WFCFG_GetIP(char* ip);

void WFCFG_ConnectAvailableAP(const char* ssid, const char* password);

void WFCFG_SmartConfigStart(bool fastSmartConfigEnabled);

void WFCFG_SmartConfigStop(void);

void WFCFG_RegisterCallback(WFCFG_Callback cb);

void WFCFG_UnRegisterCallback(void);

void WFCFG_GetRSSI(int8_t* rssi);

void WFCFG_EraseNVSInfo(void);

#endif /*__DIGO_WIFI_CONFIG_H__*/
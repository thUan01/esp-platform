#include "digo_nimble.h"
#ifdef CONFIG_NIMBLE_ENABLED


#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <esp_system.h>
#include <esp_log.h>
#include <nvs_flash.h>



#if (CONFIG_LOG_NIMBLE_ENABLED == true)

#define NIMBLE_LOGI(tag,fmt, ...)                       ESP_LOGI(tag,fmt, ##__VA_ARGS__)
#define NIMBLE_LOGE(tag,fmt, ...)                       ESP_LOGE(tag,fmt, ##__VA_ARGS__)
#define NIMBLE_LOGW(tag,fmt, ...)                       ESP_LOGW(tag,fmt, ##__VA_ARGS__)
#define NIMBLE_LOG_BUF_HEX(tag, buffer, buff_len)       ESP_LOG_BUFFER_HEX(tag, buffer, buff_len)
#elif (CONFIG_LOG_NIMBLE_ENABLED == false)

#define NIMBLE_LOGI(tag,fmt, ...)  
#define NIMBLE_LOGE(tag,fmt, ...)  
#define NIMBLE_LOGW(tag,fmt, ...)  
#define NIMBLE_LOG_BUF_HEX(tag, buffer, buff_len)

#endif /*CONFIG_LOG_NIMBLE_ENABLED*/

#define DEFAULT_BLE_DEV_NAME    "digo-ble-dev"

typedef struct ble_gatts_profile_obj ble_gatts_profile_obj_t;
typedef struct ble_gatts_char_obj ble_gatts_char_obj_t;
typedef struct ble_gatts_descr_obj ble_gatts_descr_obj_t;

struct ble_gatts_char_obj
{
    ble_dev_uuid_t char_uuid;
    uint16_t char_handle; 
    uint32_t trans_id;
    uint16_t perm;
    uint16_t property; 
    uint8_t num_desc;
    //prepare_type_env_t prepare_write_env;
    bool is_notify_enable;
    bool is_indicate_enable;
    ble_gatts_profile_obj_t* pPrf;
    ble_gatts_char_obj_t* char_next;
    ble_gatts_descr_obj_t* descr_head;

};

struct ble_gatts_descr_obj
{
    ble_dev_uuid_t descr_uuid;
    uint16_t descr_handle;
    uint32_t trans_id;
    uint16_t perm;
    ble_gatts_char_obj_t* pChar;
    ble_gatts_descr_obj_t* descr_next;
};

struct ble_gatts_profile_obj 
{
    ble_dev_uuid_t service_uuid;
    uint16_t service_handle;
    uint8_t max_handle;
    uint16_t gatts_if;
    uint16_t app_id;
    uint16_t conn_id;
    uint16_t num_char;
    //ble_gatts_cb cb;
    ble_gatts_char_obj_t* char_head;
    ble_gatts_profile_obj_t* next_prf;
};

static uint8_t g_slave_itvl_range[2];
static struct ble_hs_adv_fields *g_fields = NULL;
static struct ble_gap_adv_params *g_adv_params = NULL;
static uint8_t own_addr_type;
static const char* TAG = "digo_nimble";
static ble_gatts_callback_t callback = NULL;
static ble_gatts_profile_obj_t* prf_head = NULL;
static uint8_t ble_gatts_num_profiles = 0;

static void ble_spp_server_on_sync(void);
static void ble_spp_server_host_task(void *param);
static void ble_spp_server_on_reset(int reason);
static void gatt_svr_register_cb(struct ble_gatt_register_ctxt *ctxt, void *arg);
static int ble_server_gap_event(struct ble_gap_event *event, void *arg);
static void ble_spp_server_print_conn_desc(struct ble_gap_conn_desc *desc);
static void btc128_to_bta_uuid(ble_dev_uuid_t *p_dest, uint8_t *p_src);
// static esp_err_t bta_uuid_compare(ble_dev_uuid_t uuid1, ble_dev_uuid_t uuid2);
static ble_gatts_descr_handle_t find_descr_in_chr(ble_gatts_char_handle_t chr, const struct ble_gatt_dsc_def* dsc_def);
static uint8_t uuidType(unsigned char *p_uuid);
static void ble_print_addr(const void *addr);

void ble_store_config_init(void);
void ble_svc_gatt_init(void);

esp_err_t ble_gatts_register_callback(ble_gatts_callback_t cb)
{
    if (cb == NULL)
    {
        NIMBLE_LOGE(TAG, "null callback in %s", __func__);
        return ESP_FAIL;
    }
    callback = cb;
    return ESP_OK;
}

ble_gatts_service_handle_t ble_gatts_add_service(ble_dev_uuid_t service_uuid, 
                                                uint8_t max_handle)
{
    ble_gatts_profile_obj_t *prf = (ble_gatts_profile_obj_t*)calloc(1, sizeof(ble_gatts_profile_obj_t));
    prf->service_uuid = service_uuid;
    prf->max_handle = max_handle;
    prf->char_head = NULL;
    prf->next_prf = prf_head;
    prf->num_char = 0;
    prf_head = prf;
    prf->app_id = ble_gatts_num_profiles;
    ble_gatts_num_profiles++;
    return (ble_gatts_service_handle_t)prf;
}

ble_gatts_char_handle_t ble_gatts_add_characteristic(ble_gatts_service_handle_t service_handle, 
                                                     ble_dev_uuid_t char_uuid, 
                                                     ble_gatt_perm_t perm,
                                                     ble_gatt_prop_t property)
{
    ble_gatts_profile_obj_t *prf = (ble_gatts_profile_obj_t*)service_handle;
    ble_gatts_char_obj_t *characteristic = (ble_gatts_char_obj_t*)calloc(1, sizeof(ble_gatts_char_obj_t));
    characteristic->char_uuid = char_uuid;
    characteristic->perm = perm;
    characteristic->num_desc = 0;
    characteristic->property = property;
    characteristic->char_next = prf->char_head;
    characteristic->descr_head = NULL;
    characteristic->pPrf = prf;
    prf->char_head = characteristic;
    prf->num_char++;
    return (ble_gatts_char_handle_t)characteristic;
}

ble_gatts_descr_handle_t ble_gatts_add_descriptor(ble_gatts_char_handle_t char_handle, 
                                                ble_dev_uuid_t descr_uuid,
                                                ble_gatt_perm_t perm)
{
    ble_gatts_char_obj_t *characteristic = (ble_gatts_char_obj_t*)char_handle;
    ble_gatts_descr_obj_t *descr = (ble_gatts_descr_obj_t*)calloc(1, sizeof(ble_gatts_descr_obj_t));
    descr->descr_uuid = descr_uuid;
    descr->perm = perm;
    descr->descr_next = characteristic->descr_head;
    descr->pChar = characteristic;
    characteristic->descr_head = descr;
    characteristic->num_desc++;
    return (ble_gatts_descr_handle_t)descr;
}

/* Callback function for custom service */
static int ble_gatts_svc_handler(uint16_t conn_handle, uint16_t attr_handle,struct ble_gatt_access_ctxt *ctxt, void *arg)
{
    for (ble_gatts_profile_obj_t* prf = prf_head; prf != NULL; prf = prf->next_prf)
    {
        if (prf->conn_id == conn_handle)
        {
            for (ble_gatts_char_obj_t* chr = prf->char_head; chr != NULL; chr = chr->char_next)
            {
                if (chr->char_handle == attr_handle)
                {
                    switch(ctxt->op)
                    {
                        case BLE_GATT_ACCESS_OP_READ_CHR:
                        {
                            NIMBLE_LOGI(TAG, "Callback for read char");
                            ble_gatts_callback_param_t params;
                            params.read_char_param.service_handle = (ble_gatts_service_handle_t)prf;
                            params.read_char_param.char_handle = (ble_gatts_char_handle_t)chr; 
                            callback(BLE_GATTS_READ_CHAR, &params);
                            if (params.read_char_param.resp_val != NULL)
                            {
                                NIMBLE_LOGI(TAG, "Return Value: ");
                                NIMBLE_LOG_BUF_HEX(TAG, params.read_char_param.resp_val, params.read_char_param.resp_val_len);
                                os_mbuf_append(ctxt->om, params.read_char_param.resp_val, params.read_char_param.resp_val_len);
                            }
                        break;
                        }
                        case BLE_GATT_ACCESS_OP_WRITE_CHR:
                        {
                            NIMBLE_LOGI(TAG, "Data received in write event, conn_handle = 0x%"PRIX16",attr_handle = 0x%"PRIX16"",
                                    conn_handle,attr_handle);
                            ble_gatts_callback_param_t params;
                            params.write_char_param.service_handle = (ble_gatts_service_handle_t)prf;
                            params.write_char_param.char_handle = (ble_gatts_char_handle_t)chr;
                            char buf[255];
                            int rc = ble_hs_mbuf_to_flat(ctxt->om, buf, sizeof(buf), (uint16_t*)&params.write_char_param.len);
                            if (rc == BLE_HS_EMSGSIZE)
                            {
                                NIMBLE_LOGE(TAG, "overflow write buf size");
                            }
                            else
                            if (rc == 0)
                            {
                                params.write_char_param.value = buf;
                                //memcpy(params.write_char_param.value, buf, params.write_char_param.len);
                                callback(BLE_GATTS_WRITE_CHAR, &params);
                            }
                        break;
                        }
                        case BLE_GATT_ACCESS_OP_READ_DSC:
                        {
                           NIMBLE_LOGI(TAG, "Callback for read descr");
                            ble_gatts_callback_param_t params;
                            params.read_descr_param.service_handle = (ble_gatts_service_handle_t)prf;
                            params.read_descr_param.char_handle = (ble_gatts_char_handle_t)chr; 
                            params.read_descr_param.descr_handle = find_descr_in_chr(chr, ctxt->dsc);
                            callback(BLE_GATTS_READ_CHAR, &params);
                            if (params.read_descr_param.resp_val != NULL)
                            {
                                NIMBLE_LOGI(TAG, "Return Value: ");
                                NIMBLE_LOG_BUF_HEX(TAG, params.read_descr_param.resp_val, params.read_descr_param.resp_val_len);
                                os_mbuf_append(ctxt->om, params.read_descr_param.resp_val, params.read_descr_param.resp_val_len);
                            }
                        break;
                        }
                        case BLE_GATT_ACCESS_OP_WRITE_DSC:
                        {
                           NIMBLE_LOGI(TAG, "Data received in write descr event, conn_handle = 0x%"PRIX16",attr_handle = 0x%"PRIX16"",
                                    conn_handle, attr_handle);
                            ble_gatts_callback_param_t params;
                            params.write_descr_param.service_handle = (ble_gatts_service_handle_t)prf;
                            params.write_descr_param.char_handle = (ble_gatts_char_handle_t)chr;
                            params.write_descr_param.descr_handle = find_descr_in_chr(chr, ctxt->dsc);
                            char buf[255];
                            int rc = ble_hs_mbuf_to_flat(ctxt->om, buf, sizeof(buf), (uint16_t*)&params.write_descr_param.len);
                            if (rc == BLE_HS_EMSGSIZE)
                            {
                                NIMBLE_LOGE(TAG, "overflow write buf size");
                            }
                            else
                            if (rc == 0)
                            {
                                memcpy(params.write_descr_param.value, buf, params.write_descr_param.len);
                                callback(BLE_GATTS_WRITE_DESCR, &params);
                            }
                        break;
                        }
                        default:
                            NIMBLE_LOGI(TAG, "\nDefault Callback");
                        break;
                    }
                }   
            }
        }
    }
    
    return 0;
}

static void ble_gatts_register_service(void)
{
    struct ble_gatt_svc_def * ble_svc_gatts_defs = (struct ble_gatt_svc_def*)calloc(ble_gatts_num_profiles + 1, sizeof(struct ble_gatt_svc_def));
    uint8_t sv_count = 0;
    for (ble_gatts_profile_obj_t *prf = prf_head; prf != NULL; prf = prf->next_prf)
    {
        struct ble_gatt_chr_def* char_defs = (struct ble_gatt_chr_def*)calloc(prf->num_char + 1, sizeof(struct ble_gatt_chr_def));
        uint8_t char_count = 0;
        for (ble_gatts_char_obj_t *characteristic = prf->char_head; characteristic != NULL; characteristic = characteristic->char_next)
        {
            switch (characteristic->char_uuid.len)
            {
                case (BLE_DEV_UUID_LEN_16):
                {
                    ble_uuid16_t* uuid16 = (ble_uuid16_t*)malloc(sizeof(ble_uuid16_t));
                    uuid16->u.type = BLE_UUID_TYPE_16;
                    uuid16->value = characteristic->char_uuid.uuid.uuid16;

                    char_defs[char_count].uuid = (ble_uuid_t*)uuid16;
                    NIMBLE_LOGI(TAG, "add char uuid16: 0x%"PRIX16"", characteristic->char_uuid.uuid.uuid16);
                    break;
                }
                case (BLE_DEV_UUID_LEN_32):
                {
                    ble_uuid32_t* uuid32 = (ble_uuid32_t*)malloc(sizeof(ble_uuid32_t));
                    uuid32->u.type = BLE_UUID_TYPE_32;
                    uuid32->value = characteristic->char_uuid.uuid.uuid32;
                    char_defs[char_count].uuid = (ble_uuid_t*)uuid32;
                    NIMBLE_LOGI(TAG, "add char uuid32: 0x%"PRIX32"", characteristic->char_uuid.uuid.uuid32);
                    break;
                }
                case (BLE_DEV_UUID_LEN_128):
                {
                     ble_uuid128_t* uuid128 = (ble_uuid128_t*)malloc(sizeof(ble_uuid128_t));
                    memcpy(uuid128->value, characteristic->char_uuid.uuid.uuid128, BLE_DEV_UUID_LEN_128);
                    uuid128->u.type = BLE_UUID_TYPE_128;
                    char_defs[char_count].uuid = (ble_uuid_t*)uuid128;
                    NIMBLE_LOGI(TAG, "add char uuid128: ");
                    NIMBLE_LOG_BUF_HEX(TAG, characteristic->char_uuid.uuid.uuid128, BLE_DEV_UUID_LEN_128);
                    break;
                }
            }
            char_defs[char_count].access_cb = ble_gatts_svc_handler;
            char_defs[char_count].flags = characteristic->property;
            char_defs[char_count].val_handle = &characteristic->char_handle;
            
            if (characteristic->num_desc != 0)
            {
                uint8_t descr_count = 0;
                struct ble_gatt_dsc_def* desc_defs = (struct ble_gatt_dsc_def*)calloc(characteristic->num_desc + 1, sizeof(struct ble_gatt_dsc_def));
                for (ble_gatts_descr_obj_t *descr = characteristic->descr_head; descr != NULL; descr = descr->descr_next)
                {
                    switch (descr->descr_uuid.len)
                    {
                        case (BLE_DEV_UUID_LEN_16):
                        {
                            ble_uuid16_t* uuid16 = (ble_uuid16_t*)malloc(sizeof(ble_uuid16_t));
                            uuid16->u.type = BLE_UUID_TYPE_16;
                            uuid16->value = descr->descr_uuid.uuid.uuid16;
                            desc_defs[descr_count].uuid = (ble_uuid_t*)uuid16;
                            NIMBLE_LOGI(TAG, "add descr uuid16: 0x%"PRIX16"", descr->descr_uuid.uuid.uuid16);
                            break;
                        }
                        case (BLE_DEV_UUID_LEN_32):
                        {
                            ble_uuid32_t* uuid32 = (ble_uuid32_t*)malloc(sizeof(ble_uuid32_t));
                            uuid32->u.type = BLE_UUID_TYPE_32;
                            uuid32->value = descr->descr_uuid.uuid.uuid32;
                            desc_defs[descr_count].uuid = (ble_uuid_t*)uuid32;
                            NIMBLE_LOGI(TAG, "add descr uuid32: 0x%"PRIX32"", descr->descr_uuid.uuid.uuid32);
                            break;
                        }
                        case (BLE_DEV_UUID_LEN_128):
                        {
                            ble_uuid128_t* uuid128 = (ble_uuid128_t*)malloc(sizeof(ble_uuid128_t));
                            memcpy(uuid128->value, descr->descr_uuid.uuid.uuid128, BLE_DEV_UUID_LEN_128);
                            uuid128->u.type = BLE_UUID_TYPE_128;

                            desc_defs[descr_count].uuid = (ble_uuid_t*)uuid128;
                            NIMBLE_LOGI(TAG, "add descr uuid128: ");
                            NIMBLE_LOG_BUF_HEX(TAG, descr->descr_uuid.uuid.uuid128, BLE_DEV_UUID_LEN_128);
                            break;
                        }
                    } 
                    desc_defs[descr_count].att_flags = descr->perm;
                    desc_defs[descr_count].access_cb = ble_gatts_svc_handler;
                    descr_count++;
                }
                desc_defs[descr_count].uuid = NULL;
                char_defs[char_count].descriptors = desc_defs;
            }
            char_count++;
        }
        char_defs[char_count].uuid = NULL;
        
        ble_svc_gatts_defs[sv_count].characteristics = char_defs;
        ble_svc_gatts_defs[sv_count].type = BLE_GATT_SVC_TYPE_PRIMARY;
        
        switch (prf->service_uuid.len)
        {
            case (BLE_DEV_UUID_LEN_16):
            {
                ble_uuid16_t* uuid16 = (ble_uuid16_t*)malloc(sizeof(ble_uuid16_t));
                uuid16->u.type = BLE_UUID_TYPE_16;
                uuid16->value = prf->service_uuid.uuid.uuid16;
                ble_svc_gatts_defs[sv_count].uuid = (ble_uuid_t*)uuid16;
                NIMBLE_LOGI(TAG, "add svc uuid16: 0x%"PRIX16"", (prf->service_uuid.uuid.uuid16));
                break;
            }
            case (BLE_DEV_UUID_LEN_32):
            {
                ble_uuid32_t* uuid32 = (ble_uuid32_t*)malloc(sizeof(ble_uuid32_t));
                uuid32->u.type = BLE_UUID_TYPE_32;
                uuid32->value = prf->service_uuid.uuid.uuid32;
                ble_svc_gatts_defs[sv_count].uuid = (ble_uuid_t*)uuid32;
                NIMBLE_LOGI(TAG, "add svc uuid32: 0x%"PRIX32"", (prf->service_uuid.uuid.uuid32));
                break;
            }
            case (BLE_DEV_UUID_LEN_128):
            {
                ble_uuid128_t* uuid128 = (ble_uuid128_t*)malloc(sizeof(ble_uuid128_t));
                memcpy(uuid128->value, prf->service_uuid.uuid.uuid128, BLE_DEV_UUID_LEN_128);
                uuid128->u.type = BLE_UUID_TYPE_128;

                ble_svc_gatts_defs[sv_count].uuid = (ble_uuid_t*)uuid128;
                NIMBLE_LOGI(TAG, "add svc uuid128: ");
                NIMBLE_LOG_BUF_HEX(TAG, prf->service_uuid.uuid.uuid128, BLE_DEV_UUID_LEN_128);
                break;
            }
        }
        sv_count++;
    }
    ble_svc_gatts_defs[sv_count].type = BLE_GATT_SVC_TYPE_END;

    int rc=0;

    rc = ble_gatts_count_cfg(ble_svc_gatts_defs);

    if (rc != 0) 
    {
        NIMBLE_LOGE(TAG, "error count cfg, rc= %d", rc);
    }

    rc = ble_gatts_add_svcs(ble_svc_gatts_defs);
    if (rc != 0) 
    {
        NIMBLE_LOGE(TAG, "error add svc, rc= %d", rc);
    }
}

esp_err_t ble_gap_set_adv_data(ble_gap_adv_data_t *adv_data)
{
    if (g_fields == NULL)
    {
        g_fields = (struct ble_hs_adv_fields*)malloc(sizeof(struct ble_hs_adv_fields));
        if (g_fields == NULL)
        {
            free(g_fields);
            NIMBLE_LOGE(TAG, "%s: Out of Heap", __FUNCTION__);
            return ESP_FAIL;
        }
    }
    memset(g_fields, 0, sizeof(struct ble_hs_adv_fields));

    /*******Flags********/
    g_fields->flags = adv_data->flag;

    /*******Tx Power********/
    if (adv_data->include_txpower == true)
    {
        NIMBLE_LOGI(TAG, "Adv include Tx Power");
        g_fields->tx_pwr_lvl = BLE_HS_ADV_TX_PWR_LVL_AUTO;
        g_fields->tx_pwr_lvl_is_present = true;
    }
    
    /*******Local Name********/
    if (adv_data->include_name == true)
    {
        NIMBLE_LOGI(TAG, "Adv include Name");
        const char *name = ble_svc_gap_device_name();
        g_fields->name = (uint8_t*)name;
        g_fields->name_len = strlen(name);
        g_fields->name_is_complete = 1;
    }

    /*******Slave Interval Range********/
    if (adv_data->include_interval == true)
    {
        NIMBLE_LOGI(TAG, "Adv include Interval Range");
        g_slave_itvl_range[0] = adv_data->min_interval;
        g_slave_itvl_range[1] = adv_data->max_interval;
        g_fields->slave_itvl_range = (uint8_t*)g_slave_itvl_range;
    }

    /*******Appearance********/
    if (adv_data->include_appearance == true)
    {
        NIMBLE_LOGI(TAG, "Adv include Appearance");
        g_fields->appearance_is_present = true;
        g_fields->appearance = adv_data->appearance;
    }

    /*******Manufacturer specific data********/
    if (adv_data->manufacturer_len != 0)
    {
        NIMBLE_LOGI(TAG, "Adv include Maf");
        g_fields->mfg_data = adv_data->p_manufacturer_data;
        g_fields->mfg_data_len = (uint8_t)adv_data->manufacturer_len;
    }

    /*******Service Class UUIDS********/
    if (adv_data->service_uuid_len != 0)
    {
        NIMBLE_LOGI(TAG, "Adv include Service UUID");
        ble_uuid16_t* p_bt_uuids16 = NULL;
        ble_uuid32_t* p_bt_uuids32 = NULL;
        ble_uuid128_t* p_bt_uuid128 = NULL;
        for (uint32_t pos = 0; pos < adv_data->service_uuid_len; pos += BLE_DEV_UUID_LEN_128)
        {
            ble_dev_uuid_t bt_uuid;
            btc128_to_bta_uuid(&bt_uuid, adv_data->p_service_uuid + pos);
            NIMBLE_LOGI(TAG, "%"PRIu8"", bt_uuid.len);
            switch (bt_uuid.len)
            {
                case (BLE_DEV_UUID_LEN_16):
                {
                    if (NULL == g_fields->uuids16)
                    {
                        g_fields->uuids16 = (ble_uuid16_t*)malloc(adv_data->service_uuid_len / BLE_DEV_UUID_LEN_128 * BLE_DEV_UUID_LEN_16);
                        g_fields->num_uuids16 = 0;
                        g_fields->uuids16_is_complete = 0;
                        p_bt_uuids16 = (ble_uuid16_t*)g_fields->uuids16;
                    }
                    if (NULL != g_fields->uuids16)
                    {
                        ++g_fields->num_uuids16;
                        p_bt_uuids16->u.type = BLE_UUID_TYPE_16;
                        p_bt_uuids16->value = bt_uuid.uuid.uuid16;
                        p_bt_uuids16++;
                    }
                    NIMBLE_LOGI(TAG, "add uuid16 into adv service uuid 0x%"PRIX16"", bt_uuid.uuid.uuid16);
                    break;
                }
                case (BLE_DEV_UUID_LEN_32):
                {
                    if (NULL == g_fields->uuids32)
                    {
                        g_fields->uuids32 = (ble_uuid32_t*)malloc(adv_data->service_uuid_len / BLE_DEV_UUID_LEN_128 * BLE_DEV_UUID_LEN_32);
                        g_fields->num_uuids32 = 0;
                        g_fields->uuids32_is_complete = 0;
                        p_bt_uuids32 = (ble_uuid32_t*)g_fields->uuids32;
                    }
                    if (NULL != g_fields->uuids32)
                    {
                        ++g_fields->num_uuids32;
                        p_bt_uuids32->u.type = BLE_UUID_TYPE_32;
                        p_bt_uuids32->value = bt_uuid.uuid.uuid32;
                        p_bt_uuids32++;
                    }
                    NIMBLE_LOGI(TAG, "add uuid32 into adv service uuid 0x%"PRIX32"", bt_uuid.uuid.uuid32);
                    break;
                }
                case (BLE_DEV_UUID_LEN_128):
                {
                    // if (NULL == g_fields->uuids128)
                    // {
                        g_fields->uuids128 = (ble_uuid128_t*)malloc(sizeof(ble_uuid128_t));
                        g_fields->num_uuids128 = 0;
                        g_fields->uuids128_is_complete = false;
                        p_bt_uuid128 = (ble_uuid128_t*)(g_fields->uuids128);
                    
                    if (NULL != g_fields->uuids128)
                    {
                        p_bt_uuid128->u.type = BLE_UUID_TYPE_128;
                        memcpy(p_bt_uuid128->value, bt_uuid.uuid.uuid128, BLE_DEV_UUID_LEN_128);
                        g_fields->uuids128_is_complete = true;
                        g_fields->num_uuids128++;
                        NIMBLE_LOGI(TAG, "add uuid128 into adv service uuid: ");
                        NIMBLE_LOG_BUF_HEX(TAG, bt_uuid.uuid.uuid128, BLE_DEV_UUID_LEN_128);
                    }
                    break;
                }
            }
            if (g_fields->num_uuids16 != 0)
            {
                g_fields->uuids16_is_complete = true;
            }
            if (g_fields->num_uuids32 != 0)
            {
                g_fields->uuids32_is_complete = true;
            }
        }
    }
    return ESP_OK;
}

esp_err_t ble_gap_set_adv_params(ble_gap_adv_params_t *adv_params)
{
    if (g_adv_params == NULL)
    {
        g_adv_params = (struct ble_gap_adv_params*)malloc(sizeof(struct ble_gap_adv_params));
        if (g_adv_params == NULL)
        {
            free(g_adv_params);
            NIMBLE_LOGE(TAG, "%s: Out of Heap", __FUNCTION__);
            return ESP_FAIL;
        }
    }
    memset(g_adv_params, 0, sizeof(struct ble_gap_adv_params));

    g_adv_params->conn_mode         = adv_params->conn_mode;
    g_adv_params->disc_mode         = adv_params->disc_mode;
    g_adv_params->itvl_min          = adv_params->min_interval;
    g_adv_params->itvl_max          = adv_params->max_interval;
    g_adv_params->channel_map       = adv_params->channel_map;
    g_adv_params->filter_policy     = adv_params->filter_policy;
    g_adv_params->high_duty_cycle   = adv_params->high_duty_cycle;
    return ESP_OK;
}

esp_err_t ble_gap_start_advertising()
{
    int rc = ble_gap_adv_set_fields(g_fields);
    if (rc != 0) {
        NIMBLE_LOGE(TAG, "error setting advertisement data; rc=%d\n", rc);
        return ESP_FAIL;
    }
    rc = ble_gap_adv_start(own_addr_type, NULL, BLE_HS_FOREVER,
                                g_adv_params, ble_server_gap_event, NULL);
    if (rc != 0) {
        NIMBLE_LOGE(TAG, "error enabling advertisement; rc=%d\n", rc);
        return ESP_FAIL;
    }         
    return ESP_OK;                
}

esp_err_t ble_gap_stop_advertising(void)
{
    int rc = ble_gap_adv_stop();
    if (rc == 0)
    {
        return ESP_OK;
    }
    NIMBLE_LOGE(TAG, "Fail to stop adv, rc= %d", rc);
    return ESP_FAIL;
}

esp_err_t ble_gatts_send_notify(ble_gatts_char_handle_t char_handle, uint16_t value_len, uint8_t* value)
{
    int rc;
    struct os_mbuf *txom;
    ble_gatts_char_obj_t* characteristic = (ble_gatts_char_obj_t*)char_handle;
    txom = ble_hs_mbuf_from_flat(value, value_len);
    rc = ble_gattc_notify_custom(characteristic->pPrf->conn_id, characteristic->char_handle, txom);
    if (rc == 0)
    {
        return ESP_OK;
    }
    return ESP_FAIL;
}

esp_err_t ble_gatts_send_indicate(ble_gatts_char_handle_t char_handle, uint16_t value_len, uint8_t* value)
{
    int rc;
    struct os_mbuf *txom;
    ble_gatts_char_obj_t* characteristic = (ble_gatts_char_obj_t*)char_handle;
    txom = ble_hs_mbuf_from_flat(value, value_len);
    rc = ble_gattc_indicate_custom(characteristic->pPrf->conn_id, characteristic->char_handle, txom);
    if (rc == 0)
    {
        return ESP_OK;
    }
    return ESP_FAIL;
}

void ble_gatts_initialize(void)
{
    int rc;

    /* Initialize NVS — it is used to store PHY calibration data */
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    // ESP_ERROR_CHECK(esp_nimble_hci_and_controller_init());

    nimble_port_init();

    /* Initialize the NimBLE host configuration. */
    ble_hs_cfg.reset_cb = ble_spp_server_on_reset;
    ble_hs_cfg.sync_cb = ble_spp_server_on_sync;
    ble_hs_cfg.gatts_register_cb = gatt_svr_register_cb;
    ble_hs_cfg.store_status_cb = ble_store_util_status_rr;

    ble_hs_cfg.sm_io_cap = 3;
#ifdef CONFIG_BONDING
    ble_hs_cfg.sm_bonding = 1;
#endif
#ifdef CONFIG_MITM
    ble_hs_cfg.sm_mitm = 1;
#endif
#ifdef CONFIG_USE_SC
    ble_hs_cfg.sm_sc = 1;
#else
    ble_hs_cfg.sm_sc = 0;
#endif
#ifdef CONFIG_BONDING
    ble_hs_cfg.sm_our_key_dist = 1;
    ble_hs_cfg.sm_their_key_dist = 1;
#endif

    ble_svc_gap_init();

    ble_svc_gatt_init();

    ble_gatts_register_service();

    /* XXX Need to have template for store */
    ble_store_config_init();

    nimble_port_freertos_init(ble_spp_server_host_task);

}

void ble_gatts_dispose(ble_gatts_service_handle_t service_handle)
{

}

esp_err_t ble_gap_set_name(char* gatts_name)
{
    int rc = ble_svc_gap_device_name_set(gatts_name);
    if (rc != 0)
    {
        NIMBLE_LOGE(TAG, "Fail to set GAP name, rc= %d", rc);
        return ESP_FAIL;
    }
    return ESP_OK;
}

static int ble_server_gap_event(struct ble_gap_event *event, void *arg)
{
    struct ble_gap_conn_desc desc;
    int rc;

    switch (event->type) {
    case BLE_GAP_EVENT_CONNECT:
	/* A new connection was established or a connection attempt failed. */
        NIMBLE_LOGI(TAG, "connection %s; status=%d ",
                    event->connect.status == 0 ? "established" : "failed",
                    event->connect.status);
        if (event->connect.status == 0) {
            rc = ble_gap_conn_find(event->connect.conn_handle, &desc);
            assert(rc == 0);
            ble_spp_server_print_conn_desc(&desc);
            // is_connect=true;
            prf_head->conn_id = event->connect.conn_handle;
            ble_gatts_callback_param_t params;
            memcpy(params.connect_param.remote_bda.val, desc.peer_id_addr.val, sizeof(desc.peer_id_addr.val));
            params.connect_param.remote_bda.type = desc.peer_id_addr.type;
            callback(BLE_GATTS_CONNECT, &params);
        }
        NIMBLE_LOGI(TAG, "\n");
        if (event->connect.status != 0) {
            /* Connection failed; resume advertising. */
            callback(BLE_GATTS_CONNECT_FAIL, NULL);
        }
        return 0;

    case BLE_GAP_EVENT_DISCONNECT:
        NIMBLE_LOGI(TAG, "disconnect; reason=%d ", event->disconnect.reason);
        ble_spp_server_print_conn_desc(&event->disconnect.conn);
        ble_gatts_callback_param_t params;
        memcpy(params.disconnect_param.remote_bda.val, 
                event->disconnect.conn.peer_id_addr.val, 
                sizeof(event->disconnect.conn.peer_id_addr.val));
        params.disconnect_param.remote_bda.type = event->disconnect.conn.peer_id_addr.type;
        callback(BLE_GATTS_DISCONNECT, &params);
        return 0;

    case BLE_GAP_EVENT_CONN_UPDATE:
        /* The central has updated the connection parameters. */
        NIMBLE_LOGI(TAG, "connection updated; status=%d ",
                    event->conn_update.status);
        rc = ble_gap_conn_find(event->conn_update.conn_handle, &desc);
        assert(rc == 0);
        ble_spp_server_print_conn_desc(&desc);
        return 0;

    case BLE_GAP_EVENT_ADV_COMPLETE:
        NIMBLE_LOGI(TAG, "advertise complete; reason=%d",
                    event->adv_complete.reason);
        callback(BLE_GATTS_ADV_CMPL, NULL);
        return 0;

    case BLE_GAP_EVENT_MTU:
        NIMBLE_LOGI(TAG, "mtu update event; conn_handle=%d cid=%d mtu=%d\n",
                    event->mtu.conn_handle,
                    event->mtu.channel_id,
                    event->mtu.value);
        return 0;

    default:
	return 0;
    }
}

static void ble_spp_server_on_reset(int reason)
{
    NIMBLE_LOGE(TAG, "Resetting state; reason=%d\n", reason);
}

static void ble_spp_server_on_sync(void)
{
    int rc;

    rc = ble_hs_util_ensure_addr(0);
    assert(rc == 0);

    /* Figure out address to use while advertising (no privacy for now) */
    rc = ble_hs_id_infer_auto(0, &own_addr_type);
    if (rc != 0) {
        NIMBLE_LOGE(TAG, "error determining address type; rc=%d\n", rc);
        return;
    }

    /* Printing ADDR */
    uint8_t addr_val[6] = {0};
    rc = ble_hs_id_copy_addr(own_addr_type, addr_val, NULL);

    NIMBLE_LOGI(TAG, "Device Address: ");
    ble_print_addr(addr_val);
    ble_gatts_callback_param_t params;
    memcpy(params.ready_param.local_bda.val, addr_val, sizeof(addr_val));
    params.ready_param.local_bda.type = own_addr_type;
    callback(BLE_GATTS_READY, &params);
}

static void gatt_svr_register_cb(struct ble_gatt_register_ctxt *ctxt, void *arg)
{
    char buf[BLE_UUID_STR_LEN];

    switch (ctxt->op) {
    case BLE_GATT_REGISTER_OP_SVC:
        NIMBLE_LOGI(TAG, "registered service %s with handle=%d\n",
                    ble_uuid_to_str(ctxt->svc.svc_def->uuid, buf),
                    ctxt->svc.handle);
        break;

    case BLE_GATT_REGISTER_OP_CHR:
        NIMBLE_LOGI(TAG, "registering characteristic %s with "
                    "def_handle=%d val_handle=%d\n",
                    ble_uuid_to_str(ctxt->chr.chr_def->uuid, buf),
                    ctxt->chr.def_handle,
                    ctxt->chr.val_handle);
        break;

    case BLE_GATT_REGISTER_OP_DSC:
        NIMBLE_LOGI(TAG, "registering descriptor %s with handle=%d\n",
                    ble_uuid_to_str(ctxt->dsc.dsc_def->uuid, buf),
                    ctxt->dsc.handle);
        break;

    default:
        assert(0);
        break;
    }
}

static void ble_spp_server_host_task(void *param)
{
    NIMBLE_LOGI(TAG, "BLE Host Task Started");
    /* This function will return only when nimble_port_stop() is executed */
    nimble_port_run();

    nimble_port_freertos_deinit();
}

static void ble_spp_server_print_conn_desc(struct ble_gap_conn_desc *desc)
{
    MODLOG_DFLT(INFO, "handle=%d our_ota_addr_type=%d our_ota_addr=",
                desc->conn_handle, desc->our_ota_addr.type);
    ble_print_addr(desc->our_ota_addr.val);
    MODLOG_DFLT(INFO, " our_id_addr_type=%d our_id_addr=",
                desc->our_id_addr.type);
    ble_print_addr(desc->our_id_addr.val);
    MODLOG_DFLT(INFO, " peer_ota_addr_type=%d peer_ota_addr=",
                desc->peer_ota_addr.type);
    ble_print_addr(desc->peer_ota_addr.val);
    MODLOG_DFLT(INFO, " peer_id_addr_type=%d peer_id_addr=",
                desc->peer_id_addr.type);
    ble_print_addr(desc->peer_id_addr.val);
    MODLOG_DFLT(INFO, " conn_itvl=%d conn_latency=%d supervision_timeout=%d "
                "encrypted=%d authenticated=%d bonded=%d\n",
                desc->conn_itvl, desc->conn_latency,
                desc->supervision_timeout,
                desc->sec_state.encrypted,
                desc->sec_state.authenticated,
                desc->sec_state.bonded);
}

static const unsigned char BASE_UUID[16] = {
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80,
    0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

static uint8_t uuidType(unsigned char *p_uuid)
{

    uint8_t idx = 0;
    uint8_t match = 0;
    uint8_t all_zero = 1;
    //uuid, 16bit, [12],[13] is the value
    //uuid, 32bit, [12], [13], [14], [15] is the value

    for (idx = 0; idx != 16; ++idx) {
        if (idx == 12 || idx == 13) {
            continue;
        }

        if (p_uuid[idx] == BASE_UUID[idx]) {
            ++match;
        }

        if (p_uuid[idx] != 0) {
            all_zero = 0;
        }
    }
    if (all_zero) {
        return 0;
    }
    if (match == 12) {
        return BLE_DEV_UUID_LEN_32;
    }
    if (match == 14) {
        return BLE_DEV_UUID_LEN_16;
    }
    return BLE_DEV_UUID_LEN_128;
}

static void btc128_to_bta_uuid(ble_dev_uuid_t *p_dest, uint8_t *p_src)
{
    int i = 0;

    p_dest->len = uuidType(p_src);

    switch (p_dest->len) {
    case BLE_DEV_UUID_LEN_16:
        p_dest->uuid.uuid16 = (p_src[13] << 8) + p_src[12];
        break;

    case BLE_DEV_UUID_LEN_32:
        p_dest->uuid.uuid32  = (p_src[13] <<  8) + p_src[12];
        p_dest->uuid.uuid32 += (p_src[15] << 24) + (p_src[14] << 16);
        break;

    case BLE_DEV_UUID_LEN_128:
        for (i = 0; i != 16; ++i) {
            p_dest->uuid.uuid128[i] = p_src[i];
        }
        break;

    default:
        NIMBLE_LOGW(TAG, "%s: Unknown UUID length %"PRIu8"!", __FUNCTION__, p_dest->len);
        break;
    }
}

static ble_gatts_descr_handle_t find_descr_in_chr(ble_gatts_char_handle_t chr, const struct ble_gatt_dsc_def* dsc_def)
{
    ble_gatts_descr_handle_t descr_handle = NULL;
    ble_gatts_char_obj_t* characteristic = (ble_gatts_char_obj_t*)chr;
    for (ble_gatts_descr_obj_t* descr = characteristic->descr_head; characteristic != NULL; descr = descr->descr_next)
    {
        if (dsc_def->uuid->type == descr->descr_uuid.len)
        {
            switch (dsc_def->uuid->type)
            {
                case BLE_UUID_TYPE_16:
                {
                    if (((ble_uuid16_t*)(dsc_def->uuid))->value == descr->descr_uuid.uuid.uuid16)
                    {
                        descr_handle = (ble_gatts_descr_handle_t)descr;
                    }
                    break;
                }
                case BLE_UUID_TYPE_32:
                {
                    if (((ble_uuid32_t*)(dsc_def->uuid))->value == descr->descr_uuid.uuid.uuid32)
                    {
                        descr_handle = (ble_gatts_descr_handle_t)descr;
                    }
                    break;
                }
                case BLE_UUID_TYPE_128:
                {
                    uint8_t idx = 0;
                    for (idx = 0; idx < BLE_DEV_UUID_LEN_128; idx++)
                    {
                        if (((ble_uuid128_t*)(dsc_def->uuid))->value[idx] != descr->descr_uuid.uuid.uuid128[idx])
                        {
                            break;
                        }
                    }
                    if (idx == BLE_DEV_UUID_LEN_128)
                    {
                        descr_handle = (ble_gatts_descr_handle_t)descr;
                    }
                    break;
                }
            }
        }
    
    }
    return descr_handle;
}

// static esp_err_t bta_uuid_compare(ble_dev_uuid_t uuid1, ble_dev_uuid_t uuid2)
// {
//     if (uuid1.len != uuid2.len)
//     {
//         return ESP_FAIL;
//     }
//     switch (uuid1.len)
//     {
//         case (BLE_DEV_UUID_LEN_16):
//         {
//             return (uuid1.uuid.uuid16 == uuid2.uuid.uuid16) ? ESP_OK : ESP_FAIL;
//             break;
//         }
//         case (BLE_DEV_UUID_LEN_32):
//         {
//             return (uuid1.uuid.uuid32 == uuid2.uuid.uuid32) ? ESP_OK : ESP_FAIL;
//             break;
//         }
//         case (BLE_DEV_UUID_LEN_128):
//         {
//             for (uint8_t idx = 0; idx < BLE_DEV_UUID_LEN_128; idx++)
//             {
//                 if (uuid1.uuid.uuid128[idx] != uuid2.uuid.uuid128[idx])
//                 {
//                     return ESP_FAIL;
//                 } 
//             }
//             return ESP_OK;
//             break;
//         }
//     }
//     return ESP_OK;
// }
static void ble_print_addr(const void *addr)
{
    const uint8_t *u8p;

    u8p = addr;
    NIMBLE_LOGI(TAG, "%"PRIX8":%"PRIX8":%"PRIX8":%"PRIX8":%"PRIX8":%"PRIX8"", u8p[5], u8p[4], u8p[3], u8p[2], u8p[1], u8p[0]);
}

#endif/*CONFIG_NIMBLE_ENABLED*/

#ifndef __DIGO_NVS_STORE_H__
#define __DIGO_NVS_STORE_H__

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <esp_log.h>

typedef enum {
    NVS_OK,
    NVS_NOT_FOUND,
    NVS_KEY_NOT_FOUND,
    NVS_FAIL,
}NVS_Error_t;


/**
 * @brief NVS Initialize
 * @return NVS_FAIL when NVS partition is not found
 */
NVS_Error_t NVS_Init();


NVS_Error_t NVS_ReadStr(const char* space, const char *key, char *data, uint8_t desiredLen, uint8_t* actualLen);
NVS_Error_t NVS_ReadUInt8(const char* space, const char *key, uint8_t *value);
NVS_Error_t NVS_ReadUInt16(const char* space, const char *key, uint16_t *value);
NVS_Error_t NVS_ReadUInt32(const char* space, const char *key, uint32_t *value);
NVS_Error_t NVS_ReadUInt64(const char* space, const char *key, uint64_t *value);

NVS_Error_t NVS_WriteStr(const char* space, const char *key, const char *data);
NVS_Error_t NVS_WriteUInt8(const char* space, const char *key, uint8_t value);
NVS_Error_t NVS_WriteUInt16(const char* space, const char *key, uint16_t value);
NVS_Error_t NVS_WriteUInt32(const char* space, const char *key, uint32_t value);
NVS_Error_t NVS_WriteUInt64(const char* space, const char *key, uint64_t value);

NVS_Error_t NVS_EraseKeyInNamespace(const char* space, const char *key);
NVS_Error_t NVS_EraseAllKeysInNamespace(const char* space);
NVS_Error_t NVS_EraseAll();

#endif /*__DIGO_NVS_STORE_H__*/
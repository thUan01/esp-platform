#include "digo_time_service.h"
#include <esp_idf_version.h>
#if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4,3,0)) && (ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5,0,0))
#include "esp_sntp.h"
#elif (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(5,0,0))
#include <esp_netif_sntp.h>
#endif
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>
#include <freertos/event_groups.h>

#include <esp_bit_defs.h>
#include <inttypes.h>


#if (CONFIG_TIME_SERVICE_USE_RTC == true)
#include "digo_rtc_port.h"
#endif/*CONFIG_TIME_SERVICE_USE_RTC*/

#if (CONFIG_LOG_TIME_ENABLED == true)

#define TIME_LOGI(tag,fmt, ...)           ESP_LOGI(tag,fmt, ##__VA_ARGS__)
#define TIME_LOGE(tag,fmt, ...)           ESP_LOGE(tag,fmt, ##__VA_ARGS__)
#define TIME_LOGW(tag,fmt, ...)           ESP_LOGW(tag,fmt, ##__VA_ARGS__)

#elif (CONFIG_LOG_TIME_ENABLED == false)

#define TIME_LOGI(tag,fmt, ...)  
#define TIME_LOGE(tag,fmt, ...)  
#define TIME_LOGW(tag,fmt, ...)  

#endif /*CONFIG_LOG_TIME_ENABLED*/

#define TIME_SNTP_EVENT_GROUP_FAIL_BIT      BIT0
#define TIME_SNTP_EVENT_GROUP_SUCCESS_BIT   BIT1

#if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4,3,0)) && (ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5,0,0))
static EventGroupHandle_t TIME_SNTPEventGroup = NULL;
#endif
static TaskHandle_t TIME_SNTPTaskHandle = NULL;
static const char* TAG = "TIME_SVC";
static bool isNTPTimeAvailable = false;
static size_t mOffSetOriginalTime = 0;

static void TIME_SNTPTask(void* pvParameter);
static void TIME_SNTPSyncNotificatonCallback(struct timeval* tv);

TIME_Error_t TIME_Init(size_t offsetOriginalTime)
{
    setenv("TZ", "ICT-7", 1);
    tzset();
    mOffSetOriginalTime = offsetOriginalTime;
#if (CONFIG_TIME_SERVICE_USE_RTC == true)
    RTC_Error_t err = RTC_Init();
    if (err != RTC_OK)
    {
        return TIME_SVC_RTC_FAIL;
    }

    struct tm timeInfo;

    err = RTC_GetTime(&timeInfo);
    if (err != RTC_OK)
    {
        return TIME_SVC_RTC_FAIL;
    }
    struct timeval timeValue;
    timeValue.tv_sec = mktime(&timeInfo);
    timeValue.tv_usec = 0;
    settimeofday(&timeValue, NULL);
    TIME_LOGI(TAG, "Update Time From RTC: %"PRIu32"", (uint32_t)timeValue.tv_sec);
#endif/*CONFIG_TIME_SERVICE_USE_RTC*/
    return TIME_SVC_OK;
}

TIME_Error_t TIME_SNTPStart()
{
    #if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4,3,0)) && (ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(4,4,0))
        EventBits_t SNTPEventBits;
        sntp_setservername(0, CONFIG_TIME_SERVICE_SNTP_SERVER_NAME);
        sntp_set_time_sync_notification_cb(TIME_SNTPSyncNotificatonCallback);
        sntp_init();
    #elif (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4,4,0)) && (ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5,0,0))
        EventBits_t SNTPEventBits;
        sntp_setservername(0, CONFIG_TIME_SERVICE_SNTP_SERVER_NAME);
        sntp_set_time_sync_notification_cb(TIME_SNTPSyncNotificatonCallback);
        esp_sntp_init();
    #elif (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(5,0,0))
        esp_sntp_config_t config = ESP_NETIF_SNTP_DEFAULT_CONFIG(CONFIG_TIME_SERVICE_SNTP_SERVER_NAME);
        esp_netif_sntp_init(&config);
    #endif

    #if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4,3,0)) && (ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5,0,0))
    TIME_SNTPEventGroup = xEventGroupCreate();
    if (TIME_SNTPEventGroup == NULL)
    {
        TIME_LOGE(TAG, "Fail to Create STNP Event Group at %s(%d)", __FUNCTION__, __LINE__);
        return TIME_SVC_FAIL;
    }
    #endif

    if (TIME_SNTPTaskHandle == NULL)
    {
        xTaskCreate(TIME_SNTPTask, 
                    "SNTP Task", 
                    1024 * 3, 
                    NULL, 
                    CONFIG_TIME_SERVICE_SNTP_TASK_PRIORITY, 
                    &TIME_SNTPTaskHandle);
        return TIME_SVC_OK;
    }
    return TIME_SVC_FAIL;
}

TIME_Error_t TIME_SNTPStop()
{
    if (TIME_SNTPTaskHandle != NULL)
    {
        vTaskDelete(TIME_SNTPTaskHandle);
        return TIME_SVC_OK;
    }
    return TIME_SVC_FAIL;
}

size_t TIME_GetTimeStamp()
{
    time_t timeNow = 0;
    time(&timeNow);
    size_t offsetTimeZone = 25200;//Timezone GMT+7
    return (size_t)(timeNow) - mOffSetOriginalTime + offsetTimeZone;
}

bool TIME_IsNTPTimeAvailable()
{
    return isNTPTimeAvailable;
}

void TIME_SyncTime(size_t timestamp)
{
    struct timeval timeValue;
    timeValue.tv_sec = timestamp / 1000000L;
    timeValue.tv_usec = timestamp % 1000000L;
    settimeofday(&timeValue, NULL);
}

void TIME_ResetRTC(void)
{
    #if (CONFIG_TIME_SERVICE_USE_RTC == true)
    if (RTC_Reset() == RTC_FAIL)
    {
        TIME_LOGE(TAG, "Fail to reset RTC");
    }
    #endif
    return;
}

void TIME_GetRTCTime(struct tm* time)
{
    #if (CONFIG_TIME_SERVICE_USE_RTC == true)
    if (RTC_GetTime(time) == RTC_FAIL)
    {
        TIME_LOGE(TAG, "Fail to get RTC time");
    }
    #endif
}

static void TIME_SNTPSyncNotificatonCallback(struct timeval* tv)
{
    #if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4,3,0)) && (ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5,0,0))
    if (TIME_SNTPEventGroup != NULL)
    {
        xEventGroupSetBits(TIME_SNTPEventGroup, TIME_SNTP_EVENT_GROUP_SUCCESS_BIT);
    }
    #endif
    TIME_LOGI(TAG, "Synchronize SNTP TIME");
}

static void TIME_SNTPTask(void* pvParameter)
{   
    while(true)
    {
        #if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4,3,0)) && (ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5,0,0))
        SNTPEventBits = xEventGroupWaitBits(TIME_SNTPEventGroup, 
                                            TIME_SNTP_EVENT_GROUP_FAIL_BIT | TIME_SNTP_EVENT_GROUP_SUCCESS_BIT,
                                            pdTRUE,
                                            pdFALSE,
                                            pdMS_TO_TICKS(CONFIG_TIME_SERVICE_SNTP_WAIT_FOR_SYNC));
        if (SNTPEventBits & TIME_SNTP_EVENT_GROUP_SUCCESS_BIT)
        #elif (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(5,0,0))
        if (esp_netif_sntp_sync_wait(pdMS_TO_TICKS(CONFIG_TIME_SERVICE_SNTP_WAIT_FOR_SYNC)) == ESP_OK)
        #endif
        {
            char strTime[128] = {0};
            time_t timeNow = 0;
            struct tm timeInfo;

            isNTPTimeAvailable = true;
            time(&timeNow);
            localtime_r(&timeNow, &timeInfo);
            strftime(strTime, sizeof(strTime), "%c", &timeInfo);
            TIME_LOGI(TAG, "SNTP Synchronization, Date/Time in Vietnam: %s", strTime);
            #if (CONFIG_TIME_SERVICE_USE_RTC == true)
            RTC_SetTime(&timeInfo);
            #endif/*CONFIG_TIME_SERVICE_USE_RTC*/
        }
    }
}
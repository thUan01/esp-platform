
#include "digo_scheduler.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>

#include <nvs_flash.h>
#include <esp_partition.h>
#include <string.h>
#include <time.h>
#include <esp_log.h>
#include <inttypes.h>

#if (CONFIG_LOG_SCHEDULER_ENABLED == true)

#define SCHEDULE_LOGI(tag,fmt, ...)           ESP_LOGI(tag,fmt, ##__VA_ARGS__)
#define SCHEDULE_LOGE(tag,fmt, ...)           ESP_LOGE(tag,fmt, ##__VA_ARGS__)
#define SCHEDULE_LOGW(tag,fmt, ...)           ESP_LOGW(tag,fmt, ##__VA_ARGS__)

#elif (CONFIG_LOG_SCHEDULER_ENABLED == false)

#define SCHEDULE_LOGI(tag,fmt, ...)  
#define SCHEDULE_LOGE(tag,fmt, ...)  
#define SCHEDULE_LOGW(tag,fmt, ...)  

#endif /*CONFIG_LOG_SCHEDULE_ENABLED*/

static bool schedule_save = false;
static uint8_t schedule_count = 0;
static esp_partition_t *schedule_part = NULL;
static const char *TAG = "schedule";
static schedule_t* schedules = NULL;
static void(*schedule_callback)(char* sname, char* name, int value) = NULL;
static void(*schedule_tick_callback)(char* sname, char* name, int value, int hour, int min) = NULL;

static schedule_t* schedule_create_from_data(char* name, bool enable, int time, int repeat, bool notify);
static schedule_t* schedule_create_from_json(cJSON* jschedule);

static schedule_target_t* schedule_target_create(int power, int temp);
static void schedule_target_dispose(schedule_target_t* object);
// static int schedule_target_add(schedule_target_t* object, cJSON* target);
// static int schedule_target_edit(schedule_target_t* object, cJSON* target);
// static int schedule_target_remove(schedule_target_t* object);
static int schedule_target_export(schedule_target_t* object, char* buffer, int size);
static void schedule_target_process(schedule_t* schedule, schedule_target_t* object);
static void schedule_target_tick_process(schedule_t* schedule, schedule_target_t* object, int hour, int min);


static const esp_partition_t* find_schedule_partition(void){
    esp_err_t err = nvs_flash_init();
    ESP_ERROR_CHECK( err );
    esp_partition_iterator_t esp_partition_it = esp_partition_find(ESP_PARTITION_TYPE_DATA,ESP_PARTITION_SUBTYPE_DATA_NVS,CONFIG_SCHEDULER_PARTITION_NAME);
    if (esp_partition_it == NULL){
        SCHEDULE_LOGE(TAG,"Error find Schedule partition");
        return 0;
    }
    const esp_partition_t *schedule_part = esp_partition_get(esp_partition_it);
    SCHEDULE_LOGI(TAG,"Schedule partition found: %s ,0x%"PRIx32" ,0x%"PRIx32"  ",schedule_part->label,schedule_part->address,schedule_part->size);
    esp_partition_iterator_release(esp_partition_it);
    return schedule_part;
}

schedule_t* SCHED_Load(void) {   
    char* buffer = NULL;
    schedule_part = (esp_partition_t*)find_schedule_partition();
    if (schedule_part == NULL) return NULL;
    if (schedules != NULL) {
        schedule_t* curr = schedules;
        while (curr != NULL) {
            schedules = curr->next;
            SCHED_Dispose(curr);
            curr = schedules;
        }
    }

    do {
        buffer = (char*)malloc(CONFIG_SCHEDULER_PARTITION_SIZE);
        if (buffer == NULL) {
            SCHEDULE_LOGE(TAG,"schedule allocation failed");
            break;
        }
        buffer[CONFIG_SCHEDULER_PARTITION_SIZE - 1] = 0;
        
        if (esp_partition_read(schedule_part, 0, (uint32_t*)buffer, CONFIG_SCHEDULER_PARTITION_SIZE) != 0) {
            SCHEDULE_LOGE(TAG,"schedule read failed");
            break;
        }
        cJSON* jschedule = cJSON_Parse(buffer);
        if (jschedule == NULL) {
            SCHEDULE_LOGE(TAG,"schedule empty");
            break;
        }
        free(buffer);
        buffer = NULL;
      
        SCHED_Add(schedules, jschedule);
        cJSON_Delete(jschedule);
        schedule_save = false;
    } while (0);
    if (buffer)
        free(buffer);
    return schedules;
}

void SCHED_Dispose(schedule_t* object) {
    if (object->target != NULL)
        schedule_target_dispose(object->target);
    if (object->name)
        free(object->name);
    free(object);
}
int SCHED_GetNumber(void){
    return schedule_count;
}
int SCHED_Add(schedule_t* object, cJSON* jschedule) {
    int index_max = 0; 
    cJSON* current = NULL;
    schedule_t* schedule = NULL;
    if (SCHED_GetNumber() == CONFIG_SCHEDULER_COUNT_LIMIT)
    {
        return ESP_FAIL;
    }
    if (cJSON_GetArraySize(jschedule) >= CONFIG_SCHEDULER_COUNT_LIMIT)  index_max = 5;
    else index_max = cJSON_GetArraySize(jschedule);
    for (int index = index_max; index > 0; index--) {
        SCHEDULE_LOGI(TAG,"number schedule add %d",cJSON_GetArraySize(jschedule));
        current = cJSON_GetArrayItem(jschedule, index - 1);
        schedule = schedule_create_from_json(current);
        if (schedule != NULL) {
            schedule->next = schedules;
            schedules = schedule;
            schedule_save = true;
            schedule_count ++;
        }
        else
        {
            SCHEDULE_LOGI(TAG,"target create failed ");
        } 

    }
    return schedule_save ? ESP_OK : ESP_FAIL;
}

int SCHED_Edit(schedule_t* object, cJSON* jschedule) {
    cJSON* name;
    cJSON* current;
    schedule_t* prev = NULL;
    schedule_t* curr = NULL;
    schedule_t* schedule = NULL;
    
    for (int index = 0; index < cJSON_GetArraySize(jschedule); index++) {
        current = cJSON_GetArrayItem(jschedule, index);
        name = cJSON_GetObjectItem(current, "code");
        curr = schedules;
        prev = NULL;
        SCHEDULE_LOGI(TAG,"%s",curr->name);
        while (curr != NULL) {
            if (strcmp(curr->name, name->valuestring) == 0) {
                if (prev == NULL)
                    schedules = curr->next;
                else
                    prev->next = curr->next;
                SCHEDULE_LOGI(TAG,"schedule remove -> %s", curr->name);
                schedule_count--;
                SCHED_Dispose(curr);
                schedule_save = true;
                break;
            }
            prev = curr;
            curr = curr->next;
        }

        schedule = schedule_create_from_json(current);
        if (schedule != NULL) {
            if (prev == NULL) {
                schedule->next = schedules;
                schedules = schedule;
            }
            else {
                schedule->next = prev->next;
                prev->next = schedule;
            }
            schedule_save = true;
            SCHEDULE_LOGI(TAG,"schedule edit -> %s", name->valuestring);
            schedule_count ++;
        }
    }

    // SCHED_Remove(object, jschedule);
    // SCHED_Add(schedules, jschedule);
    return schedule_save ? 0 : -1;
}

int SCHED_Remove(schedule_t* object, cJSON* jschedule) {
    cJSON* name;
    cJSON* current;
    schedule_t* prev = NULL;
    schedule_t* curr = NULL;
    
    for (int index = 0; index < cJSON_GetArraySize(jschedule); index++) {
        current = cJSON_GetArrayItem(jschedule, index);
        name = cJSON_GetObjectItem(current, "code");
        curr = schedules;
        SCHEDULE_LOGI(TAG,"%s",curr->name);
        while (curr != NULL) {
            if (strcmp(curr->name, name->valuestring) == 0) {
                if (prev == NULL)
                    schedules = curr->next;
                else
                    prev->next = curr->next;
                SCHEDULE_LOGI(TAG,"schedule remove -> %s", curr->name);
                schedule_count --;
                SCHED_Dispose(curr);
                schedule_save = true;
                break;
            }
            prev = curr;
            curr = curr->next;
        }
    }
    return schedule_save ? 0 : -1;
}

void disable_timeout_cb(TimerHandle_t xTimer)
{
    char* sname = (char*)pvTimerGetTimerID(xTimer);
    schedule_t* curr = schedules;
    while (curr != NULL) {
        if (strcmp(curr->name, sname) == 0)
        {
            if (curr->enable == false)
            {
                curr->enable = true;
                schedule_save = true;
            }
            break;
        }
        curr = curr->next;
    }
}

int SCHED_Disable(char* sname, uint8_t minute) {
    int reval = ESP_OK;
    schedule_t* curr = schedules;
    while (curr != NULL) {
        if (strcmp(curr->name, sname) == 0)
        {
            if (curr->enable == true)
            {
                curr->enable = false;
                schedule_save = true;
                xTimerCreate("disable timeout",
                            pdMS_TO_TICKS(minute * 3600),
                            pdFALSE,
                            (void*)sname,
                            disable_timeout_cb);
            }
            break;
        }
        curr = curr->next;
    }
    SCHEDULE_LOGI(TAG, "schedule disable %s", curr->name);
    return reval;
}

#define YEAR0 1900 /* the first year */
#define EPOCH_YR 1970 /* EPOCH = Jan 1 1970 00:00:00 */
#define SECS_DAY (24L * 60L * 60L)
#define LEAPYEAR(year) (!((year) % 4) && (((year) % 100) || !((year) % 400)))
#define YEARSIZE(year) (LEAPYEAR(year) ? 366 : 365)
#define FIRSTSUNDAY(timp) (((timp)->tm_yday - (timp)->tm_wday + 420) % 7)
#define FIRSTDAYOF(timp) (((timp)->tm_wday - (timp)->tm_yday + 420) % 7)

const int _ytab[2][12] = {
		{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
		{ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }};

struct tm* m_gmtime(const time_t* ts) {
	static struct tm br_time;
	struct tm *timep = &br_time;
	time_t time = *ts;
	unsigned long dayclock, dayno;
	int year = YEAR0;
	dayclock = (unsigned long)time % SECS_DAY;
	dayno = (unsigned long)time / SECS_DAY;
	timep->tm_sec = dayclock % 60;
	timep->tm_min = (dayclock % 3600) / 60;
	timep->tm_hour = dayclock / 3600;
	timep->tm_wday = (dayno + 4) % 7; /* day 0 was a thursday */
	{
		unsigned long zz = dayno / 1461; // 365+365+365+366 Breaks beyond 2100
		if (zz > 32) zz = 32; // fix 2101-2106
		year += zz * 4;
		dayno -= zz * 1461;
	}
	while (dayno >= YEARSIZE(year)) {
		dayno -= YEARSIZE(year);
		year++;
	}
	timep->tm_year = year - YEAR0;
	timep->tm_yday = dayno;
	timep->tm_mon = 0;
	while (dayno >= _ytab[LEAPYEAR(year)][timep->tm_mon]) {
		dayno -= _ytab[LEAPYEAR(year)][timep->tm_mon];
		timep->tm_mon++;
	}
	timep->tm_mday = dayno + 1;
	timep->tm_isdst = 0;
	return timep;
}

void SCHED_Process(schedule_t* object, uint32_t timestamp) {
    schedule_t* head = schedules;
    // uint32_t ts = timestamp;
    time_t now = 0;
    time(&now);
    struct tm *dt = malloc(sizeof(struct tm));
    localtime_r(&now,dt);
    uint8_t wday[7] = {1, 2, 3, 4, 5, 6, 0};
    int min = 0;
    int hour = 0;

    if (schedule_save) {
        SCHED_SaveToFlash();
        schedule_save = false;
    }

    while (head != NULL) {
        hour = head->time / 100;
        min = head->time % 100;
        
        if (head->enable && (hour == dt->tm_hour) && (min == dt->tm_min)) {
            if (head->repeat == 0) {
                head->enable = 0;
                schedule_save = true;
            }
            else if ((head->repeat & (1 << wday[dt->tm_wday & 0x07])) == 0) {
                head->execute = false;
            }
            if (head->execute) {
                head->execute = false;
                schedule_target_process(head, head->target);
            }
        }
        else if (head->enable && (dt->tm_sec <= 10)) {
            if (head->execute) {
                head->execute = false;
                schedule_target_tick_process(head, head->target, hour, min);
            }
        }
        else
            head->execute = true;
        head = head->next;
    }
    free(dt);
}

void SCHED_RegisterSchedCallback(void(*callback)(char* sname, char* name, int value)) {
    schedule_callback = callback;
}

void SCHED_RegisterSchedTickCallback(void(*callback)(char* sname, char* name, int value, int hour, int min)) {
    schedule_tick_callback = callback;
}

void SCHED_SaveToFlash(void) {
    int size;
    char* buffer = NULL;
    do {
        if (schedules == NULL) {
            SCHEDULE_LOGE(TAG,"schedule empty");
            break;
        }

        buffer = (char*)malloc(CONFIG_SCHEDULER_PARTITION_SIZE);
        if (buffer == NULL) {
            SCHEDULE_LOGE(TAG,"schedule allocation failed");
            break;
        }

        memset(buffer, 0, CONFIG_SCHEDULER_PARTITION_SIZE);
        size = SCHED_Export(schedules, buffer, CONFIG_SCHEDULER_PARTITION_SIZE - 1);
        if (size <= 0) {
            SCHEDULE_LOGE(TAG,"schedule export failed");
            break;
        }
        
        if (esp_partition_erase_range(schedule_part, 0, CONFIG_SCHEDULER_PARTITION_SIZE) != 0) {
            SCHEDULE_LOGE(TAG,"schedule erase failed");
            break;
        }
        
        if (esp_partition_write(schedule_part, 0, (uint32_t*)buffer, size + 1) != 0) {
            SCHEDULE_LOGE(TAG,"schedule write failed");
            break;
        }
        SCHEDULE_LOGI(TAG,"schedule save success");
    } while (0);
    
    if (buffer != NULL)
        free(buffer);
}


int SCHED_DeleteAll(void) {
    int reval = ESP_OK;
    schedule_t* curr = schedules;
    while (curr != NULL) {
        schedules = curr->next;
        SCHED_Dispose(curr);
        curr = schedules;
    }
    schedules = NULL;
    schedule_count = 0;
    reval = esp_partition_erase_range(schedule_part, 0, CONFIG_SCHEDULER_PARTITION_SIZE);
    SCHEDULE_LOGI(TAG, "schedule delete all -> %d", reval);
    return reval;
}

int SCHED_EnableAll(void) {
    int reval = ESP_OK;
    schedule_t* curr = schedules;
    while (curr != NULL) {
        if (curr->enable == false) {
            curr->enable = true;
            schedule_save = true;
        }
        curr = curr->next;
    }
    SCHEDULE_LOGI(TAG, "schedule enable all");
    return reval;
}

int SCHED_DisableAll(void) {
    int reval = ESP_OK;
    schedule_t* curr = schedules;
    while (curr != NULL) {
        if (curr->enable == true) {
            curr->enable = false;
            schedule_save = true;
        }
        curr = curr->next;
    }
    SCHEDULE_LOGI(TAG, "schedule disable all");
    return reval;
}

int SCHED_Export(schedule_t* object, char* buffer, int size) {
    int count = 0;
    bool first = true;
    const char* bstring[2] = {"false", "true"};
    
    object = schedules;
    count += sprintf(buffer, "[");
    while (object != NULL) {
        if (first) {
            count += sprintf(&buffer[count], "{\"code\":\"%s\",\"enable\":%s,\"time\":\"%04ld\",\"repeat\":%"PRIu32",\"notify\":%s,\"target\":",\
            object->name, bstring[object->enable], object->time, object->repeat, bstring[object->notify]);
            count += schedule_target_export(object->target, &buffer[count], size - count);
            count += sprintf(&buffer[count], "}");
            first = false;
        }
        else {
            count += sprintf(&buffer[count], ",{\"code\":\"%s\",\"enable\":%s,\"time\":\"%04ld\",\"repeat\":%"PRIu32",\"notify\":%s,\"target\":",\
            object->name, bstring[object->enable], object->time, object->repeat, bstring[object->notify]);
            count += schedule_target_export(object->target, &buffer[count], size - count);
            count += sprintf(&buffer[count], "}");
        }
        if (count >= size)
        {
            SCHEDULE_LOGE(TAG,"schedule export failed");
        }
        object = object->next;
    }
    count += sprintf(&buffer[count], "]");
    // logi("schedule export: %s", buffer);
    return count;
}

static schedule_target_t* schedule_target_create(int power, int temp) {
    schedule_target_t* object = (schedule_target_t*)malloc(sizeof(schedule_target_t));
    object->power = power;
    object->temp  = temp;
    return object;
}

static void schedule_target_dispose(schedule_target_t* object) {
    free(object);
}

static void schedule_target_process(schedule_t* schedule, schedule_target_t* object) {
    SCHEDULE_LOGI(TAG,"schedule active -> %s", schedule->name);
    if (schedule_callback != NULL) {
        schedule_callback(schedule->name, (char*)"switch", object->power);
        schedule_callback(schedule->name,(char*)"notify",schedule->notify);
        schedule_callback(schedule->name, (char*)"set_temp", object->temp);
    }
}

static void schedule_target_tick_process(schedule_t* schedule, schedule_target_t* object, int hour, int min) {
    SCHEDULE_LOGI(TAG,"schedule tick -> %s", schedule->name);
    if (schedule_tick_callback != NULL) {
        schedule_tick_callback(schedule->name, (char*)"set_temp", object->temp, hour, min);
        schedule_tick_callback(schedule->name, (char*)"switch", object->power, hour, min); 
    }
}

static int schedule_target_export(schedule_target_t* object, char* buffer, int size) {
    const char* bstring[2] = {"false", "true"};
    return sprintf(buffer, "{\"switch\":%s,\"set_temp\":%d}", bstring[object->power], object->temp);
}

static schedule_t* schedule_create_from_json(cJSON* jschedule) {
    cJSON* power = NULL;
    cJSON* temp = NULL;
    schedule_t* schedule = NULL;

    cJSON* name = cJSON_GetObjectItem(jschedule, "code");
    cJSON* time_p = cJSON_GetObjectItem(jschedule, "time");
    cJSON* enable = cJSON_GetObjectItem(jschedule, "enable");
    cJSON* repeat = cJSON_GetObjectItem(jschedule, "repeat");
    cJSON* notify = cJSON_GetObjectItem(jschedule,"notify");
    cJSON* target = cJSON_GetObjectItem(jschedule, "target");
    if (target != NULL) {
        power = cJSON_GetObjectItem(target, "switch");
        temp = cJSON_GetObjectItem(target, "set_temp");
    }
    if ((name != NULL) && (time_p != NULL) && (enable != NULL) && (repeat != NULL) && (notify != NULL) && (power != NULL) && (temp != NULL)) {
        schedule = schedule_create_from_data(name->valuestring, enable->valueint, atoi(time_p->valuestring), repeat->valueint, notify->valueint);
        if (schedule != NULL) {
            schedule->target = schedule_target_create(power->valueint, temp->valueint);
            SCHEDULE_LOGI(TAG,"schedule create -> %s", name->valuestring);
        }
    }
    return schedule;
}

static schedule_t* schedule_create_from_data(char* name, bool enable, int time, int repeat, bool notify) {
    schedule_t* object = (schedule_t*)malloc(sizeof(schedule_t));
    if (object != NULL) {  
        object->time = time;
        object->execute = true;
        object->enable = enable;
        object->repeat = repeat;
        object->notify = notify;
        object->next = NULL;
        object->target = NULL;
        object->name = (char*)malloc(strlen(name) + 1);
        memcpy((void*)(object->name), (const void*)name, strlen(name)+1);
    }
    return object;
}

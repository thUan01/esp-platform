#include "digo_wifi_config.h"

#include <time.h>

#include <esp_wifi.h>
#include <esp_wifi_default.h>
#include <esp_wpa2.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>

#include <esp_netif.h>
#include <esp_smartconfig.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include <freertos/event_groups.h>

#include "digo_time_service.h"
#include "digo_nvs_store.h"
#include "digo_syslog.h"

#if (CONFIG_LOG_WIFICONFIG_ENABLED == true)

#define WIFICONFIG_LOGI(tag,fmt, ...)           ESP_LOGI(tag,fmt, ##__VA_ARGS__)
#define WIFICONFIG_LOGE(tag,fmt, ...)           ESP_LOGE(tag,fmt, ##__VA_ARGS__)
#define WIFICONFIG_LOGW(tag,fmt, ...)           ESP_LOGW(tag,fmt, ##__VA_ARGS__)

#elif (CONFIG_LOG_WIFICONFIG_ENABLED == false)

#define WIFICONFIG_LOGI(tag,fmt, ...)  
#define WIFICONFIG_LOGE(tag,fmt, ...)  
#define WIFICONFIG_LOGW(tag,fmt, ...)  

#endif /*CONFIG_LOG_WIFICONFIG_ENABLED*/


#define WFCFG_CONNECTED_BIT         BIT0
#define WFCFG_ESPTOUCH_DONE_BIT     BIT1
#define WFCFG_FAIL_BIT              BIT2
#define WFCFG_STATION_READY_BIT     BIT3

typedef struct {
    EventGroupHandle_t eventGroup;
    wifi_config_t config;
    bool connected;
    bool smartconfigEnabled;
    bool fastSmartConfigEnabled;
    uint8_t retry;
    char hostName[32];
    int8_t rssi;
    char ip[32];
}WFCFG_Typedef;


static WFCFG_Typedef WIFIConfig;
static const char *TAG = "WIFI_CONFIG";
static uint8_t u8ReconectCount = 0;
static esp_netif_t *stationNetIF;
static WFCFG_Callback callback = NULL;
TaskHandle_t SmartConfigTaskHandle = NULL;
TimerHandle_t SmartconfigScanDoneTimeoutTimerHandle = NULL;

static void OnScanDoneTimeoutHandler(TimerHandle_t xTimers);
static void SmartConfigTask(void *pvParameter);
static void WIFIConnectTask(void *pvParameter);

static void WFCFG_EventHandler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        if (callback != NULL)
        {
            callback(WFCFG_EVT_STA_START, NULL);
        }
        if (WIFIConfig.smartconfigEnabled == false)
        {
            esp_wifi_connect();
        }
        else
        {
            if (WIFIConfig.fastSmartConfigEnabled == false)
            {
                xTaskCreate(SmartConfigTask, "SmartConfigTask", 4096, NULL, 3, &SmartConfigTaskHandle);
            }
        }
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (WIFIConfig.connected == true)
        {
            WIFIConfig.connected = false;
        }
        esp_wifi_connect();
        if (u8ReconectCount < WIFIConfig.retry)
        {
            u8ReconectCount++;
            WIFICONFIG_LOGI(TAG, "Connecting to [\"%s\"] [\"%s\"]......", 
                            WIFIConfig.config.sta.ssid,
                            WIFIConfig.config.sta.password);
        }
        else
        {
            WIFICONFIG_LOGI(TAG, "Connect fail");
            if (callback != NULL)
            {
                callback(WFCFG_EVT_STA_DISCONNECTED, NULL);
            }
            u8ReconectCount = 0;
            if (WIFIConfig.smartconfigEnabled == true)
            {
                xEventGroupSetBits(WIFIConfig.eventGroup, WFCFG_FAIL_BIT);
            }
        }
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        if (WIFIConfig.connected == false)
        {
            if (callback != NULL)
                callback(WFCFG_EVT_STA_CONNECTED, NULL);
            WIFIConfig.connected = true;
            if (WIFIConfig.smartconfigEnabled == true)
            {
                xEventGroupSetBits(WIFIConfig.eventGroup, WFCFG_CONNECTED_BIT);
            }
        }
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
        if (callback != NULL)
        {
            WFCFG_CallbackParam_t param;
            char IPStr[32];
            uint8_t len = sprintf(IPStr, IPSTR, IP2STR(&event->ip_info.ip));
            IPStr[len] = '\0';
            param.ipv4.ip = IPStr; 
            memcpy(WIFIConfig.ip, IPStr, len);
            callback(WFCFG_EVT_STA_GOT_IPv4, &param);
        }
        u8ReconectCount = 0;
    }
    else if (event_base == SC_EVENT && event_id == SC_EVENT_SCAN_DONE)
    {
        if (SmartconfigScanDoneTimeoutTimerHandle != NULL)
        {
            if (xTimerIsTimerActive(SmartconfigScanDoneTimeoutTimerHandle) == pdTRUE)
            {
                xTimerStop(SmartconfigScanDoneTimeoutTimerHandle, 0);
            }
        }
        
        if (callback != NULL)
        {
            callback(WFCFG_SC_EVT_SCAN_DONE, NULL);
        }
        WIFICONFIG_LOGI(TAG, "Scan done");
    }
    else if (event_base == SC_EVENT && event_id == SC_EVENT_FOUND_CHANNEL)
    {
        if (callback != NULL)
        {
            callback(WFCFG_SC_EVT_FOUND_CHANNEL, NULL);
        }
        WIFICONFIG_LOGI(TAG, "Found channel");
    }
    else if (event_base == SC_EVENT && event_id == SC_EVENT_GOT_SSID_PSWD)
    {
        WIFICONFIG_LOGI(TAG, "Got SSID and password");
        smartconfig_event_got_ssid_pswd_t *evt = (smartconfig_event_got_ssid_pswd_t *)event_data;
        uint8_t ssid[33] = {0};
        uint8_t password[65] = {0};

        bzero(&WIFIConfig.config, sizeof(wifi_config_t));
        memcpy(WIFIConfig.config.sta.ssid, evt->ssid, sizeof(WIFIConfig.config.sta.ssid));
        memcpy(WIFIConfig.config.sta.password, evt->password, sizeof(WIFIConfig.config.sta.password));
        WIFIConfig.config.sta.bssid_set = evt->bssid_set;

        if (WIFIConfig.config.sta.bssid_set == true)
        {
            memcpy(WIFIConfig.config.sta.bssid, evt->bssid, sizeof(WIFIConfig.config.sta.bssid));
        }

        memcpy(ssid, evt->ssid, sizeof(evt->ssid));
        memcpy(password, evt->password, sizeof(evt->password));

        WIFICONFIG_LOGI(TAG, "SSID:%s", ssid);
        WIFICONFIG_LOGI(TAG, "PASSWORD:%s", password);

        if (callback != NULL)
        {
            WFCFG_CallbackParam_t param;
            param.scAPInfo.ssid = (char*)ssid;
            param.scAPInfo.pwd = (char*)password;
            callback(WFCFG_SC_EVT_GOT_SSID_PSWD, &param);
        }
        esp_wifi_disconnect();
        esp_wifi_set_config(WIFI_IF_STA, &WIFIConfig.config);
        esp_wifi_connect();
    }
    else if (event_base == SC_EVENT && event_id == SC_EVENT_SEND_ACK_DONE)
    {
        if (callback != NULL)
        {
            callback(WFCFG_SC_EVT_SEND_ACK_DONE, NULL);
        }
        xEventGroupSetBits(WIFIConfig.eventGroup, WFCFG_ESPTOUCH_DONE_BIT);
    }
}

void WFCFG_RegisterCallback(WFCFG_Callback cb)
{
    callback = cb;
}

void WFCFG_UnRegisterCallback(void)
{
    if (callback != NULL)
    {
        callback = NULL;
    }
}

void WFCFG_NetIFInit(void)
{
    esp_netif_init();
    esp_event_loop_create_default();

    stationNetIF = esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    esp_wifi_init(&cfg);
}

void WFCFG_Init(const char* hostName, uint8_t reconnectRetry)
{
    sprintf(WIFIConfig.hostName, "%s", hostName);
    WIFIConfig.retry = reconnectRetry;
    WIFIConfig.connected = false;
    WIFIConfig.smartconfigEnabled = false;

    WIFIConfig.eventGroup = xEventGroupCreate();

}

void WFCFG_SmartConfigStart(bool fastSmartConfigEnabled)
{

    WIFIConfig.smartconfigEnabled = true;
    WIFIConfig.fastSmartConfigEnabled = fastSmartConfigEnabled;

    esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &WFCFG_EventHandler, NULL);
    esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &WFCFG_EventHandler, NULL);
    esp_event_handler_register(SC_EVENT, ESP_EVENT_ANY_ID, &WFCFG_EventHandler, NULL);

    esp_wifi_set_mode(WIFI_MODE_STA);
    esp_wifi_start();

    if (fastSmartConfigEnabled == true)
    {
        WIFICONFIG_LOGI(TAG, "Running Fast Smartconfig");
        xTaskCreate(SmartConfigTask, "SmartConfigTask", 4096, NULL, 3, &SmartConfigTaskHandle);
    }
    else
    {
        WIFICONFIG_LOGI(TAG, "Running Normal SmartConfig");
        SmartconfigScanDoneTimeoutTimerHandle = xTimerCreate("Common WIFI Timer",
                                                            pdMS_TO_TICKS(CONFIG_WIFICONFIG_SCAN_DONE_TIMEOUT),
                                                            pdFALSE,
                                                            NULL,
                                                            OnScanDoneTimeoutHandler);
    }
}


void WFCFG_SmartConfigStop(void)
{
    if (WIFIConfig.smartconfigEnabled == true)
    {
        esp_smartconfig_stop();

        vEventGroupDelete(WIFIConfig.eventGroup);
        if (SmartConfigTaskHandle != NULL)
        {
            vTaskDelete(SmartConfigTaskHandle);
        }
    }
}

void WFCFG_DeInit(void)
{
    esp_wifi_deinit();
}

bool WFCFG_CheckAPInfoOnNVS(char* ssid, char* password)
{
    uint8_t ssidLen = 0, passwordLen = 0;
    uint8_t ssidRet = NVS_ReadStr( CONFIG_WIFICONFIG_NVS_NAMESPACE, 
                                    CONFIG_WIFICONFIG_NVS_KEY_SSID, 
                                    ssid,
                                    32,
                                    &ssidLen);
    ssid[ssidLen] = '\0';
    uint8_t passRet = NVS_ReadStr( CONFIG_WIFICONFIG_NVS_NAMESPACE,
                                    CONFIG_WIFICONFIG_NVS_KEY_PWD, 
                                    password,
                                    32,
                                    &passwordLen);
    password[passwordLen] = '\0';
    return ((ssidRet == NVS_OK) && (passRet == NVS_OK));
}

void WFCFG_ConnectAvailableAP(const char* ssid, const char* password)
{
    esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &WFCFG_EventHandler, NULL);
    esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &WFCFG_EventHandler, NULL);
    esp_event_handler_register(SC_EVENT, ESP_EVENT_ANY_ID, &WFCFG_EventHandler, NULL);
    if ((ssid != NULL) && (password != NULL))
    {
        sprintf((char*)WIFIConfig.config.sta.ssid, "%s", ssid);
        sprintf((char*)WIFIConfig.config.sta.password, "%s", password);
    }
    WIFICONFIG_LOGI(TAG, "Connecting to WIFI [\"%s\"] [\"%s\"]", 
                    WIFIConfig.config.sta.ssid,
                    WIFIConfig.config.sta.password);

    esp_wifi_set_mode(WIFI_MODE_STA);
    esp_wifi_set_config(WIFI_IF_STA, &WIFIConfig.config);
    esp_wifi_start();
}

uint8_t WFCFG_GetIP(char* ip)
{
    uint8_t len = sprintf(ip, "%s", WIFIConfig.ip);
    ip[len] = '\0';
    return len;
}

uint8_t WFCFG_GetSSID(char* ssid)
{
    uint8_t len = sprintf(ssid, "%s", WIFIConfig.config.sta.ssid);
    ssid[len] = '\0';
    return len;
}

void WFCFG_GetRSSI(int8_t* rssi)
{
    if (WIFIConfig.connected == true)
    {
        wifi_ap_record_t apInfo;
        esp_wifi_sta_get_ap_info(&apInfo);
        *rssi = apInfo.rssi;
    }
}

void WFCFG_EraseNVSInfo(void)
{
    NVS_EraseKeyInNamespace(CONFIG_WIFICONFIG_NVS_NAMESPACE, CONFIG_WIFICONFIG_NVS_KEY_SSID);
    NVS_EraseKeyInNamespace(CONFIG_WIFICONFIG_NVS_NAMESPACE, CONFIG_WIFICONFIG_NVS_KEY_PWD);
}


static void OnScanDoneTimeoutHandler(TimerHandle_t xTimers)
{
    callback(WFCFG_SC_EVT_SCAN_DONE_TIMEOUT, NULL);
}


static void SmartConfigTask(void *pvParameter)
{
    EventBits_t uxBits;
    esp_smartconfig_set_type(SC_TYPE_ESPTOUCH);
    smartconfig_start_config_t cfg = SMARTCONFIG_START_CONFIG_DEFAULT();
    esp_smartconfig_start(&cfg);
    
    if ((SmartconfigScanDoneTimeoutTimerHandle != NULL) && (WIFIConfig.fastSmartConfigEnabled == false))
    {
        xTimerStart(SmartconfigScanDoneTimeoutTimerHandle, 0);
    }
    while (true)
    {
        uxBits = xEventGroupWaitBits(WIFIConfig.eventGroup, 
                                    WFCFG_CONNECTED_BIT | WFCFG_ESPTOUCH_DONE_BIT | WFCFG_FAIL_BIT, 
                                    true, 
                                    false, 
                                    portMAX_DELAY);

        if (uxBits & WFCFG_CONNECTED_BIT)
        {
            WFCFG_EraseNVSInfo();
            WIFICONFIG_LOGI(TAG, "Connected to AP [\"%s\"] [\"%s\"]", WIFIConfig.config.sta.ssid,
                 WIFIConfig.config.sta.password);
            SYSLOG_Save(CAUSE_APP_CONFIG, VALUE_SUCCESS, EXTRA_SMARTCF);
            WIFIConfig.connected = 1;
            NVS_WriteStr(CONFIG_WIFICONFIG_NVS_NAMESPACE, CONFIG_WIFICONFIG_NVS_KEY_SSID, (char*)WIFIConfig.config.sta.ssid);
            NVS_WriteStr(CONFIG_WIFICONFIG_NVS_NAMESPACE, CONFIG_WIFICONFIG_NVS_KEY_PWD, (char*)WIFIConfig.config.sta.password);
        }
        else
        if (uxBits & WFCFG_FAIL_BIT)
        {
            SYSLOG_Save(CAUSE_APP_CONFIG, VALUE_FAIL, EXTRA_SMARTCF);
            SYSLOG_Save(CAUSE_REBOOT, VALUE_SUCCESS, EXTRA_SMARTCF);
            WIFICONFIG_LOGE(TAG, "Can't connect to Wifi [\"%s\"] [\"%s\"]", 
                            WIFIConfig.config.sta.ssid,
                            WIFIConfig.config.sta.password);
            WIFICONFIG_LOGE(TAG, "Wrong SSID and Password, restart ");
            vTaskDelay(1000);
            esp_restart();
        }
        else
        if (uxBits & WFCFG_ESPTOUCH_DONE_BIT)
        {
            WIFICONFIG_LOGI(TAG, "Smart Config over. Delete task");
            WIFIConfig.smartconfigEnabled = false;
            esp_smartconfig_stop();
            vTaskDelete(NULL);
        }
    }
}



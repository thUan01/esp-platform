#ifndef __DIGO_CONFIG_H__
#define __DIGO_CONFIG_H__

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>
#include <esp_log.h>

typedef enum {
    DIGOCFG_OK = 0,
    DIGOCFG_FAIL,
}DIGOCFG_Error_t;

typedef enum {
    DIGOCFG_API_InfoGetEvent,                   /**< Event on Infomation Get Method on HTTP Server */
    DIGOCFG_API_SetupPostSuccessEvent,          /**< Event on Successfully Setup Post Method on HTTP Server */
    DIGOCFG_API_SetupPostFailEvent,             /**<Event on Fail Setup Post Method on HTTP Server */
}DIGOCFG_CallbackEvent_t;

typedef struct DIGOCFG_Info DIGOCFG_Info_t;

struct DIGOCFG_Info{
    char host[64];
    uint8_t hostLen;

    uint16_t port;
    #ifdef CONFIG_MQTT_USE_OLD_SERVER
    char user[64];
    uint8_t userLen;

    char pwd[64];
    uint8_t pwdLen;

    #else

    char tenantID[128];
    uint8_t tenantIDLen;

    char deviceID[128];
    uint8_t deviceIDLen;

    uint16_t deviceProfileID;

    char token[128];
    uint8_t tokenLen;

    #endif
    bool isSSLUsed;
};
    
typedef union {
    struct DIGOCFG_Info setupSuccess;
}DIGOCFG_CallbackParam_t;

typedef void (*DIGOCFG_Callback_t)(DIGOCFG_CallbackEvent_t event, DIGOCFG_CallbackParam_t* param);


/**
 * @brief DIGO Configure Protocol Start
 * @param id            Device ID. Ex: 123456
 * @param productID     Product ID. Ex: WHWF.123456 
 * @param firmware      Firmware Version
 * @param hardware      Hardware Version
 * @param mac           MAC Address
 * @return DIGOCFG_OK when start HTTP success, DIGOCFG_FAIL if vice and versa
 */
DIGOCFG_Error_t DIGOCFG_Start(uint32_t id, const char* productID, uint16_t firmware, uint16_t hardware, const char* mac);


/**
 * @brief DIGO Configure Protocol Stop
 * @param none
 * @return DIGOCFG_OK when stop HTTP success, DIGOCFG_FAIL if vice and versa
 */
DIGOCFG_Error_t DIGOCFG_Stop(void);


/**
 * @brief Check Info of DIGO Configure Protocol on NVS
 * @param info          store info if it's available on NVS
 * @return DIGOCFG_OK when it's available on NVS, DIGOCFG_FAIL if vice and versa
 */
DIGOCFG_Error_t DIGOCFG_CheckInfoOnNVS(DIGOCFG_Info_t* info);


/**
 * @brief Get TenantID 
 * @param tenantID      device tenant ID
 * @return DIGOCFG_FAIL if parameter is invalid.
 * @note input tenant ID have to be allocated first
 */
DIGOCFG_Error_t DIGOCFG_GetTenantID(char* tenantID);


/**
 * @brief Get DeviceID 
 * @param deviceID      device ID
 * @return DIGOCFG_FAIL if parameter is invalid.
 * @note input device ID have to be allocated first
 */
DIGOCFG_Error_t DIGOCFG_GetDeviceID(char* deviceID);


/**
 * @brief Get Device Profile ID 
 * @param profileID      device profile ID
 * @return DIGOCFG_FAIL if parameter is invalid.
 * @note input profile ID have to be allocated first
 */
DIGOCFG_Error_t DIGOCFG_GetDeviceProfileID(uint16_t* profileID);


/**
 * @brief Get token 
 * @param token      device token
 * @return DIGOCFG_FAIL if parameter is invalid.
 * @note token have to be allocated first
 */
DIGOCFG_Error_t DIGOCFG_GetToken(char* token);


/**
 * @brief Register callback  
 * @param cb        callback in <DIGOCFG_Callback_t>
 * @return DIGOCFG_FAIL if parameter is invalid.
 * @note cb have to be a declear function first.
 */
DIGOCFG_Error_t DIGOCFG_RegisterCallback(DIGOCFG_Callback_t cb);


/**
 * @brief Erase all infomation saved on NVS
 * @param none
 * @return DIGOCFG_FAIL if fail to erase infomation on NVS, DIGOCFG_OK if vice and versa.
 */
DIGOCFG_Error_t DIGOCFG_EraseNVSInfo(void);

#endif/*__DIGO_MQTT_CONFIG_H__*/
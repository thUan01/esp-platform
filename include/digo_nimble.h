#ifndef __DIGO_NIMBLE_H__
#define __DIGO_NIMBLE_H__

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <esp_log.h>

#ifdef CONFIG_NIMBLE_ENABLED

#include "esp_nimble_hci.h"
#include "nimble/nimble_port.h"
#include "nimble/nimble_port_freertos.h"
#include "host/ble_hs.h"
#include "host/util/util.h"
#include "host/ble_gatt.h"
#include "console/console.h"
#include "services/gap/ble_svc_gap.h"

typedef void* ble_gatts_service_handle_t;
typedef void* ble_gatts_char_handle_t;
typedef void* ble_gatts_descr_handle_t; 

typedef enum {
    BLE_GATT_PERM_READ              = BLE_GATT_CHR_F_READ,
    BLE_GATT_PERM_READ_ENCRYPTED    = BLE_GATT_CHR_F_READ_ENC,
    BLE_GATT_PERM_READ_AUTHOR       = BLE_GATT_CHR_F_READ_AUTHOR,
    BLE_GATT_PERM_WRITE             = BLE_GATT_CHR_F_WRITE,
    BLE_GATT_PERM_WRITE_ENCRYPTED   = BLE_GATT_CHR_F_WRITE_ENC,
    BLE_GATT_PERM_WRITE_AUTHOR      = BLE_GATT_CHR_F_WRITE_AUTHOR,
}ble_gatt_perm_t;

typedef enum {
    BLE_GATT_PROP_BROADCAST         = BLE_GATT_CHR_PROP_BROADCAST,
    BLE_GATT_PROP_READ              = BLE_GATT_CHR_PROP_READ,
    BLE_GATT_PROP_WRITE_NO_RSP      = BLE_GATT_CHR_PROP_WRITE_NO_RSP,
    BLE_GATT_PROP_WRITE             = BLE_GATT_CHR_PROP_WRITE,
    BLE_GATT_PROP_NOTIFY            = BLE_GATT_CHR_PROP_NOTIFY,
    BLE_GATT_PROP_INDICATE          = BLE_GATT_CHR_PROP_INDICATE,
    BLE_GATT_PROP_AUTH              = BLE_GATT_CHR_PROP_AUTH_SIGN_WRITE,
    BLE_GATT_PROP_EXT_PROP          = BLE_GATT_CHR_PROP_EXTENDED
}ble_gatt_prop_t;

typedef enum {
    BLE_GAP_CON_MODE_NONE           = BLE_GAP_CONN_MODE_NON,
    BLE_GAP_CON_MODE_DIR            = BLE_GAP_CONN_MODE_DIR,
    BLE_GAP_CON_MODE_UND            = BLE_GAP_CONN_MODE_UND,
}ble_gap_conn_mode_t;

typedef enum {
    BLE_GAP_DIS_MODE_NONE           = BLE_GAP_DISC_MODE_NON,
    BLE_GAP_DIS_MODE_LTD            = BLE_GAP_DISC_MODE_LTD,
    BLE_GAP_DIS_MODE_GEN            = BLE_GAP_DISC_MODE_GEN,
}ble_gap_disc_mode_t;

typedef enum {
    BLE_ADV_FLAG_LIMIT_DISC         = BLE_HS_ADV_F_DISC_LTD,
    BLE_ADV_FLAG_GEN_DISC           = BLE_HS_ADV_F_DISC_GEN,
    BLE_ADV_FLAG_BREDR_NOT_SPT      = BLE_HS_ADV_F_BREDR_UNSUP,
    BLE_ADV_FLAG_DMT_CRTL_SPT       = 0,
    BLE_ADV_FLAG_DMT_HOST_SPT       = 0,
    BLE_ADV_FLAG_NON_LIMIT_DISC     = 0,
}ble_adv_data_flag_t;


typedef enum {
    BLE_ADDRESS_PUBLIC                 = BLE_OWN_ADDR_PUBLIC,
    BLE_ADDRESS_RANDOM                 = BLE_OWN_ADDR_RANDOM,
    BLE_ADDRESS_RPA_PUBLIC             = BLE_OWN_ADDR_RPA_PUBLIC_DEFAULT,
    BLE_ADDRESS_RPA_RANDOM             = BLE_OWN_ADDR_RANDOM,
}ble_addr_type_t;
typedef struct  
{
#define BLE_DEV_UUID_LEN_16         2
#define BLE_DEV_UUID_LEN_32         4
#define BLE_DEV_UUID_LEN_128        16

    uint8_t len;
    union {
        uint16_t uuid16;
        uint32_t uuid32;
        uint8_t uuid128[BLE_DEV_UUID_LEN_128];
    }uuid;
}ble_dev_uuid_t;

typedef struct {
    ble_gap_conn_mode_t     conn_mode;
    ble_gap_disc_mode_t     disc_mode;
    uint16_t                min_interval;
    uint16_t                max_interval;
    uint8_t                 channel_map;
    uint8_t                 filter_policy;
    uint8_t                 high_duty_cycle;
}ble_gap_adv_params_t;

typedef struct {
    uint8_t         flag;

    bool            include_name;

    bool            include_txpower;

    int             min_interval;
    int             max_interval;
    bool            include_interval;

    int             appearance;
    bool            include_appearance;

    uint16_t        manufacturer_len;
    uint8_t         *p_manufacturer_data;

    uint16_t        service_data_len;
    uint8_t         *p_service_data;


    uint16_t        service_uuid_len;
    uint8_t         *p_service_uuid;
    
    bool            set_scan_rsp;

}ble_gap_adv_data_t;

typedef enum {
    BLE_GATTS_READ_CHAR = 0,
    BLE_GATTS_WRITE_CHAR,
    BLE_GATTS_READ_DESCR,
    BLE_GATTS_WRITE_DESCR,
    BLE_GATTS_READY,
    BLE_GATTS_ADV_CMPL,
    BLE_GATTS_CONNECT,
    BLE_GATTS_CONNECT_FAIL,
    BLE_GATTS_DISCONNECT
}ble_gatts_callback_event_t;

typedef union {
    struct ble_gatts_read_char{
        ble_gatts_service_handle_t service_handle;
        ble_gatts_char_handle_t char_handle;
        char* resp_val;
        uint8_t resp_val_len;
    }read_char_param;
    struct ble_gatts_write_char {
        ble_gatts_service_handle_t service_handle;
        ble_gatts_char_handle_t char_handle;
        char *value;
        uint8_t len;
    }write_char_param;
    struct ble_gatts_read_descr{
        ble_gatts_service_handle_t service_handle;
        ble_gatts_char_handle_t char_handle;
        ble_gatts_descr_handle_t descr_handle;
        char* resp_val;
        uint8_t resp_val_len;
    }read_descr_param;
    struct ble_gatts_write_descr {
        ble_gatts_service_handle_t service_handle;
        ble_gatts_char_handle_t char_handle;
        ble_gatts_descr_handle_t descr_handle;
        char *value;
        uint8_t len;
    }write_descr_param;
    struct ble_gatts_ready{
        ble_addr_t local_bda;
    }ready_param;
    struct ble_gatts_connect {
        ble_addr_t remote_bda;
    }connect_param;
    struct ble_gatts_disconnect {
        ble_addr_t remote_bda;
    }disconnect_param;
}ble_gatts_callback_param_t;

typedef void (*ble_gatts_callback_t)(ble_gatts_callback_event_t event, ble_gatts_callback_param_t *param);

ble_gatts_service_handle_t ble_gatts_add_service(ble_dev_uuid_t service_uuid, uint8_t max_handle);
ble_gatts_char_handle_t ble_gatts_add_characteristic(ble_gatts_service_handle_t service_handle, 
                                                     ble_dev_uuid_t char_uuid, 
                                                     ble_gatt_perm_t perm,
                                                     ble_gatt_prop_t property);
ble_gatts_descr_handle_t ble_gatts_add_descriptor(ble_gatts_char_handle_t char_handle, 
                                                ble_dev_uuid_t descr_uuid,
                                                ble_gatt_perm_t perm);
  
void ble_gatts_initialize(void);   
void ble_gatts_on(void); 
void ble_gatts_dispose(ble_gatts_service_handle_t service_handle);                                             
esp_err_t ble_gap_set_name(char* gatts_name);
esp_err_t ble_gap_set_adv_data(ble_gap_adv_data_t *adv_data);
esp_err_t ble_gap_set_adv_params(ble_gap_adv_params_t *adv_params);
esp_err_t ble_gap_set_scan_rsp_data(ble_gap_adv_data_t *scan_rsp_data);
esp_err_t ble_gap_start_advertising(void);
esp_err_t ble_gap_stop_advertising(void);
esp_err_t ble_gatts_register_callback(ble_gatts_callback_t cb);
// esp_err_t ble_gatts_send_char_response(ble_gatts_char_handle_t char_handle, esp_gatt_status_t status, uint8_t* value, uint16_t len);
// esp_err_t ble_gatts_send_descr_response(ble_gatts_descr_handle_t descr_handle, esp_gatt_status_t status, uint8_t* value, uint16_t len);
esp_err_t ble_gatts_send_notify(ble_gatts_char_handle_t char_handle, uint16_t value_len, uint8_t* value);
esp_err_t ble_gatts_send_indicate(ble_gatts_char_handle_t char_handle, uint16_t value_len, uint8_t* value);

#endif /*CONFIG_NIMBLE_ENABLED*/
#endif /*__DIGO_NIMBLE_H__*/




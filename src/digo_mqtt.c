#include "digo_mqtt.h"
#include "digo_time_service.h"
#include "digo_syslog.h"
#include "digo_nvs_store.h"

#include <mqtt_client.h>
#include <esp_idf_version.h>
#include <inttypes.h>

#if (CONFIG_LOG_MQTT_ENABLED == true)

#define MQTT_LOGI(tag,fmt, ...)           ESP_LOGI(tag,fmt, ##__VA_ARGS__)
#define MQTT_LOGE(tag,fmt, ...)           ESP_LOGE(tag,fmt, ##__VA_ARGS__)
#define MQTT_LOGW(tag,fmt, ...)           ESP_LOGW(tag,fmt, ##__VA_ARGS__)

#elif (CONFIG_LOG_MQTT_ENABLED == false)

#define MQTT_LOGI(tag,fmt, ...)  
#define MQTT_LOGE(tag,fmt, ...)  
#define MQTT_LOGW(tag,fmt, ...)  

#endif /*CONFIG_LOG_MQTT_ENABLED*/

typedef struct {
    bool connected;
}MQTTCLI_Instance_t;

MQTTCLI_Instance_t MQTTInstace;
static const char *TAG = "MQTT_CLIENT";
static esp_mqtt_client_handle_t MQTTClient = NULL;

static MQTT_Callback_t callback = NULL;

static void log_error_if_nonzero(const char * message, int error_code)
{
    if (error_code != 0) {
        MQTT_LOGE(TAG, "Last error %s: 0x%x", message, error_code);
    }
}

static void MQTT_EventHandler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) 
{
    esp_mqtt_event_handle_t event = event_data;

    switch(event_id)
    {
        case (MQTT_EVENT_CONNECTED):
        {
            if (MQTTInstace.connected == false)
            {
                MQTTInstace.connected = true;
                if (callback != NULL)
                {
                    callback(MQTTCLI_CONNECTED_EVT, NULL);
                }
            }
            break;
        }
        case MQTT_EVENT_DISCONNECTED:
        {
            if (MQTTInstace.connected != false)
            {
                MQTTInstace.connected = false;
                if (callback != NULL)
                {
                    callback(MQTTCLI_DISCONNECTED_EVT, NULL);
                }
            }
            break;
        }
        case MQTT_EVENT_SUBSCRIBED:
        {
            if (callback != NULL)
            {
                MQTTCLI_CallbackParam_t param;
                param.sub.msgID = event->msg_id;
                callback(MQTTCLI_SUBSCRIBED_EVT, &param);
            }
            break;
        }
        case MQTT_EVENT_PUBLISHED:
        {
            if (callback != NULL)
            {
                MQTTCLI_CallbackParam_t param;
                param.pub.msgID = event->msg_id;
                callback(MQTTCLI_PUBLISHED_EVT, &param);
            }
            break;
        }
        case MQTT_EVENT_DATA:
        {
            if (callback != NULL)
            {
                MQTTCLI_CallbackParam_t param;
                param.data.topic = event->topic;
                param.data.topicLen = event->topic_len;
                param.data.payload = event->data;
                param.data.len = event->data_len;
                callback(MQTTCLI_DATA_EVT, &param);
            }
            break;
        }
        case MQTT_EVENT_ERROR:
        {
            if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT)
            {
                MQTT_LOGE(TAG, "MQTT_ERROR_TYPE_TCP_TRANSPORT");
                log_error_if_nonzero("reported from esp-tls", event->error_handle->esp_tls_last_esp_err);
                log_error_if_nonzero("reported from tls stack", event->error_handle->esp_tls_stack_err);
                log_error_if_nonzero("captured as transport's socket errno", event->error_handle->esp_transport_sock_errno);
                MQTT_LOGI(TAG, "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));
            }
            if (event->error_handle->error_type == MQTT_ERROR_TYPE_CONNECTION_REFUSED)
            {
                MQTT_LOGE(TAG, "MQTT_ERROR_TYPE_CONNECTION_REFUSED, return code %d", event->error_handle->connect_return_code);
            }
            break;
        }
    }
}

void MQTT_RegisterCallback(MQTT_Callback_t cb){
    callback = cb;
}

void MQTT_UnRegisterCallback()
{
    callback = NULL;
}
MQTTCLI_Error_t MQTT_Init(MQTTCLI_Info_t info)
{
#if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4,3,0)) && (ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5,0,0))
    esp_mqtt_client_config_t cfg = {
        .host = info.host,
        .port = info.port,
        .username = info.user,
        .password = info.password,
        .client_id = info.clientID,
        .transport = MQTT_TRANSPORT_OVER_TCP,
        .buffer_size = 4096,
    };
#elif (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(5,0,0))
    esp_mqtt_client_config_t cfg = {
        .broker = {
            .address.hostname = info.host,
            .address.port = info.port,
            .address.transport = MQTT_TRANSPORT_OVER_TCP,
        },
        .credentials.username = info.user,
        .credentials.authentication.password = info.password,
        .credentials.client_id = info.clientID,
        .session.keepalive = CONFIG_MQTTCLI_KEEP_ALIVE_INTERVAL,
    };
#endif
    if (info.isSSL == true)
    {
        if (info.certPem != NULL)
        {
            #if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4,3,0)) && (ESP_IDF_VERSION < ESP_IDF_VERSION_VAL(5,0,0))
            cfg.cert_pem = info.certPem;
            cfg.transport = MQTT_TRANSPORT_OVER_SSL;
            #elif (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(5,0,0))
            cfg.broker.verification.certificate = info.certPem;
            cfg.broker.address.transport = MQTT_TRANSPORT_OVER_SSL;
            #endif
        }
    }
    MQTTClient = esp_mqtt_client_init(&cfg);
    esp_err_t err = esp_mqtt_client_register_event(MQTTClient, ESP_EVENT_ANY_ID, MQTT_EventHandler, NULL);
    if (err != ESP_OK)
    {
        MQTT_LOGE(TAG, "Fail to Register MQTT Event, rsn = %s at %s(%d)",esp_err_to_name(err), __FUNCTION__, __LINE__);
        return MQTTCLI_FAIL;
    }

    return MQTTCLI_OK;
}

MQTTCLI_Error_t MQTT_Start(void)
{
    if (MQTTClient != NULL)
    {
        esp_err_t err = esp_mqtt_client_start(MQTTClient);
        if (err != ESP_OK)
        {
            MQTT_LOGE(TAG, "Fail to Start MQTT, rsn = %s at %s(%d)",esp_err_to_name(err), __FUNCTION__, __LINE__);
            return MQTTCLI_FAIL;
        }
    }
    else
    {
        MQTT_LOGE(TAG, "MQTT Client Can't be Found");
        return MQTTCLI_FAIL;
    }
    return MQTTCLI_OK;
}

MQTTCLI_Error_t MQTT_Stop(void)
{
    if (MQTTClient != NULL)
    {
        esp_err_t err = esp_mqtt_client_stop(MQTTClient);
        if (err != ESP_OK)
        {
            MQTT_LOGE(TAG, "Fail to Stop MQTT, rsn = %s at %s(%d)",esp_err_to_name(err), __FUNCTION__, __LINE__);
            return MQTTCLI_FAIL;
        }
    }
    return MQTTCLI_OK;
}

MQTTCLI_Error_t MQTT_Pub(const char* topic, const char* data, MQTTCLI_QOS_t qos, bool retain)
{
    uint32_t msgID = 0;
    if (MQTTClient != NULL)
    {
        int err = esp_mqtt_client_publish(MQTTClient, topic, data, strlen(data), qos, retain);
        if (err == -1)
        {
            MQTT_LOGE(TAG, "Fail to Publist MQTT Topic [%s] at %s(%d)",topic, __FUNCTION__, __LINE__);
            return MQTTCLI_FAIL;
        }
        msgID = err;
    }
    else
    {
        MQTT_LOGE(TAG, "MQTT Client Can't be Found");
        return MQTTCLI_FAIL;
    }
    MQTT_LOGI(TAG, "Published Topic[%"PRIu32"][%s]", msgID, topic);
    return MQTTCLI_OK;
}


MQTTCLI_Error_t MQTT_Subscribe(const char* topic, MQTTCLI_QOS_t qos)
{
    uint32_t msgID = 0;
    if (MQTTClient != NULL)
    {
        int err = esp_mqtt_client_subscribe(MQTTClient, topic, qos);
        if (err == -1)
        {
            MQTT_LOGE(TAG, "Fail to Subscribe MQTT Topic [%s] at %s(%d)", topic, __FUNCTION__, __LINE__);
            return MQTTCLI_FAIL;
        }
        msgID = err;
    }
    else
    {
        MQTT_LOGE(TAG, "MQTT Client Can't be Found");
        return MQTTCLI_FAIL;
    }
    MQTT_LOGI(TAG, "Subscribed Topic[%"PRIu32"][%s]", msgID, topic);
    return MQTTCLI_OK;
}





#include "digo_nimble.h"
#include "mqtt.h"
#include "nvs_store.h"
#include "scheduler.h"
#include "syslog.h"
#include "time_service.h"
#include "wifi_config.h"

#include "unity.h"
#include "test_utils.h"
#include <inttypes.h>

#define TEST_WIFI_SSID      "DIGO_T4"
#define TEST_WIFI_PWD       "DIGO@2021"

#define TEST_MQTT_HOST      "digo-vinawind-mqtt.digotech.net"
#define TEST_MQTT_PORT      4004
#define TEST_MQTT_USER      "superuser"
#define TEST_MQTT_PASSWORD  "897D15C038DC00CAAA0A3F38FEE32321"
#define TEST_MQTT_CLIENTID  "d81dfd6dff224925b79d243c2e6ba9b5"
#define TEST_MQTT_TOPIC     "test"

static const char* TAG = "TEST_PLATFORM";
static bool isUseSmartConfig = false;
static bool isTestMQTT = false;

extern const uint8_t ca_pem_start[]     asm("_binary_ca_pem_start");
extern const uint8_t ca_pem_end[]       asm("_binary_ca_pem_end");

static void MQTTCallback(MQTTCLI_CallbackEvent_t event, MQTTCLI_CallbackParam_t* param);

static void WIFICallback(WFCFG_CallbackEvent_t event, WFCFG_CallbackParam_t* param)
{
    switch(event)
    {
        case WFCFG_SC_EVT_GOT_SSID_PSWD:
        {
            ESP_LOGI(TAG, "SmartConfig Got SSID & PWD");
            isUseSmartConfig = true;
            break;
        }
        case WFCFG_EVT_STA_GOT_IPv4:
        {
            int8_t rssi = 0;
            ESP_LOGI(TAG, "Got IPv4: %s", param->ipv4.ip);
            WFCFG_GetRSSI(&rssi);
            ESP_LOGI(TAG, "RSSI: %d", rssi);
            TIME_SNTPStart();
            if (isTestMQTT == true)
            {
                MQTTCLI_Info_t cfg = {
                    .host = TEST_MQTT_HOST,
                    .port = TEST_MQTT_PORT,
                    .user = TEST_MQTT_USER,
                    .password = TEST_MQTT_PASSWORD,
                    .clientID = TEST_MQTT_CLIENTID,
                    .certPem = (const char*)ca_pem_start,
                    .isSSL = true
                };

                MQTT_Init(cfg);
                MQTT_RegisterCallback(MQTTCallback);
                MQTT_Start(cfg);
                ESP_LOGI(TAG, "<<<<Start MQTT>>>>");
            }
            else
            if (isUseSmartConfig == false)
            {
                //WFCFG_DeInit();
            }
        }
        default:
        {
            break;
        }
    }
}

static void MQTTCallback(MQTTCLI_CallbackEvent_t event, MQTTCLI_CallbackParam_t* param)
{
    switch (event)
    {
        case MQTTCLI_CONNECTED_EVT:
        {
            ESP_LOGI(TAG, "MQTT Connected");
            MQTT_Subscribe(TEST_MQTT_TOPIC, MQTTCLI_QOS1);
            break;
        }
        case MQTTCLI_DISCONNECTED_EVT:
        {
            ESP_LOGI(TAG, "MQTT Disconnected");
            break;
        }
        case MQTTCLI_SUBSCRIBED_EVT:
        {
            ESP_LOGI(TAG, "Subscribed on Topic[%"PRIu32"]", param->sub.msgID);
            MQTT_Pub(TEST_MQTT_TOPIC, "a dungdz vipro", MQTTCLI_QOS1, false);
            break;
        }
        case MQTTCLI_PUBLISHED_EVT:
        {
            ESP_LOGI(TAG, "<---- Published on Topic[%"PRIu32"]", param->pub.msgID);
            break;
        }
        case MQTTCLI_DATA_EVT:
        {
            char buf[256];
            uint8_t len = snprintf(buf, param->data.len, "%s", param->data.payload);
            buf[len] = '\0';
            ESP_LOGI(TAG, "----> Got Data [%s][len = %d] on Topic[%s]", buf, len, param->data.topic);
            ESP_LOGI(TAG, "<<<<Stop MQTT>>>>");
            break;
        }
        default:
        {
            break;
        }
    }
}

TEST_CASE("wifi test", "wifi")
{
    char ssid[64];
    char password[64];
    NVS_Init();
    SYSLOG_Init();
    TIME_Init(946684800);
    WFCFG_RegisterCallback(WIFICallback);
    WFCFG_Init("TEST_WIFI", 10);
    if (WFCFG_CheckAPInfoOnNVS(ssid, password) == true)
    {
        ESP_LOGI(TAG, "Connect to <%s>/<%s>", ssid, password);
        WFCFG_ConnectAvailableAP(ssid, password);
    }
    else
    {
        WFCFG_SmartConfigStart();
    }
}

TEST_CASE("mqtt test", "mqtt")
{
    NVS_Init();
    SYSLOG_Init();
    TIME_Init(946684800);
    WFCFG_Init("TEST_WIFI", 10);
    WFCFG_RegisterCallback(WIFICallback);
    WFCFG_ConnectAvailableAP(TEST_WIFI_SSID, TEST_WIFI_PWD);
    
    isTestMQTT = true;
}

TEST_CASE("time test", "time")
{
    NVS_Init();
    SYSLOG_Init();

    WFCFG_Init("TEST_WIFI", 10);
    WFCFG_RegisterCallback(WIFICallback);
    WFCFG_ConnectAvailableAP(TEST_WIFI_SSID, TEST_WIFI_PWD);
    //Offset 30 years, it's origin is from 2000 rather than 1970
    TIME_Init(946684800);
    while (TIME_IsNTPTimeAvailable() == false)
    {
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
    ESP_LOGI(TAG, "%d", TIME_GetTimeStamp());
}

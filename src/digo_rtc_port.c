#include "digo_rtc_port.h"
#if (CONFIG_TIME_SERVICE_RTC_TYPE == RTC_TYPE_PCF8563)
#include "rtc_pcf8563.h"
#endif/*CONFIG_TIME_SERVICE_RTC_TYPE*/

RTC_Error_t RTC_Init()
{
#if (CONFIG_TIME_SERVICE_RTC_TYPE == RTC_TYPE_PCF8563)
    if (PCF8563_Init() != ESP_OK)
    {
        return RTC_FAIL;
    }
    return RTC_OK;
#endif/*CONFIG_TIME_SERVICE_RTC_TYPE*/
}


RTC_Error_t RTC_Reset()
{
#if (CONFIG_TIME_SERVICE_RTC_TYPE == RTC_TYPE_PCF8563)
    if (PCF8563_Reset() != ESP_OK)
    {
        return RTC_FAIL;
    }
    return RTC_OK;
#endif/*CONFIG_TIME_SERVICE_RTC_TYPE*/
}


RTC_Error_t RTC_SetTime(struct tm* time)
{
#if (CONFIG_TIME_SERVICE_RTC_TYPE == RTC_TYPE_PCF8563)
    if (PCF8563_SetTime(time) != ESP_OK)
    {
        return RTC_FAIL;
    }
    return RTC_OK;
#endif/*CONFIG_TIME_SERVICE_RTC_TYPE*/
}

RTC_Error_t RTC_GetTime(struct tm* time)
{
#if (CONFIG_TIME_SERVICE_RTC_TYPE == RTC_TYPE_PCF8563)
    if (PCF8563_GetTime(time) != ESP_OK)
    {
        return RTC_FAIL;
    }
    return RTC_OK;
#endif/*CONFIG_TIME_SERVICE_RTC_TYPE*/
}
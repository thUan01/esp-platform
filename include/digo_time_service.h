#ifndef __DIGO_TIME_SERVICE_H__
#define __DIGO_TIME_SERVICE_H__

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <esp_log.h>

#include <time.h>
#include <sys/time.h>

typedef enum
{
    TIME_SVC_OK,
    TIME_SVC_RTC_FAIL,
    TIME_SVC_FAIL
}TIME_Error_t;

TIME_Error_t TIME_Init(size_t offsetOriginalTime);
TIME_Error_t TIME_SNTPStart();
TIME_Error_t TIME_SNTPStop();
size_t TIME_GetTimeStamp();
void TIME_SyncTime(size_t timestamp);
bool TIME_IsNTPTimeAvailable();
void TIME_ResetRTC(void);
void TIME_GetRTCTime(struct tm* time);

#endif/*__DIGO_TIME_SERVICE_H__*/
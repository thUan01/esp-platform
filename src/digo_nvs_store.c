#include "digo_nvs_store.h"

#include <nvs_flash.h>
#include <esp_partition.h>
#include <esp_log.h>
#include <inttypes.h>
static const char* TAG = "NVS_STORE";

#if (CONFIG_LOG_NVS_ENABLED == true)

#define NVS_LOGI(tag,fmt, ...)           ESP_LOGI(tag,fmt, ##__VA_ARGS__)
#define NVS_LOGE(tag,fmt, ...)           ESP_LOGE(tag,fmt, ##__VA_ARGS__)
#define NVS_LOGW(tag,fmt, ...)           ESP_LOGW(tag,fmt, ##__VA_ARGS__)

#elif (CONFIG_LOG_NVS_ENABLED == false)

#define NVS_LOGI(tag,fmt, ...)  
#define NVS_LOGE(tag,fmt, ...)  
#define NVS_LOGW(tag,fmt, ...)  

#endif /*CONFIG_LOG_NVS_ENABLED*/
typedef struct NVS_Namspace_t NVS_Namspace_t;
struct NVS_Namspace_t{
    char name[16];
    NVS_Namspace_t* next;
};

static NVS_Namspace_t* NVS_NamespaceHead = NULL;
NVS_Error_t NVS_Init(){
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    esp_partition_iterator_t partitionIT = esp_partition_find(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_NVS, "nvs");
    if (partitionIT == NULL){
        NVS_LOGE(TAG,"Error find NVS partition");
        return NVS_FAIL;
    }
    else {
        const esp_partition_t *pPartition = esp_partition_get(partitionIT);
        NVS_LOGI(TAG,"NVS partition found: %s, 0x%"PRIX32", 0x%"PRIX32"",pPartition->label, pPartition->address, pPartition->size);
        esp_partition_iterator_release(partitionIT);
    }    
    return NVS_OK;
}


static esp_err_t NVS_OpenNameSpace(const char* namespace, nvs_handle_t *out_handle){
    NVS_Namspace_t* object = (NVS_Namspace_t*)malloc(sizeof(NVS_Namspace_t));
    sprintf(object->name, "%s", namespace);
    object->next = NVS_NamespaceHead;
    NVS_NamespaceHead = object;
    return nvs_open(namespace, NVS_READWRITE, out_handle);
}


NVS_Error_t NVS_ReadStr(const char* namespace, const char *key, char *data, uint8_t desiredLen, uint8_t* actualLen){
    nvs_handle_t nvs_handle;
    uint8_t tempActualLen = 0;
    if (desiredLen == 0)
    {
        desiredLen = 64;
    }
    if (actualLen == NULL)
    {
        actualLen = &tempActualLen;
    }
    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_get_str(nvs_handle, key, NULL, actualLen);
    if (*actualLen > desiredLen)
    {
        ESP_LOGI(TAG, "Overflow Read Key %s/%s", namespace, key);
        return NVS_FAIL;
    }
    err = nvs_get_str(nvs_handle, key, data, actualLen);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Read Key %s/%s Fail, rsn=%s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    nvs_close(nvs_handle);
    return NVS_OK;
}


NVS_Error_t NVS_WriteStr(const char* namespace, const char *key, const char *data){
    nvs_handle_t nvs_handle;

    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_set_str(nvs_handle,key,data);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Write Key %s/%s Fail, rsn = %s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_commit(nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Commit Fail, rsn=%s, %s(%d)", esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }
    NVS_LOGI(TAG, "Wrote [%s] into [%s/%s]", data, namespace, key);
    nvs_close(nvs_handle);
    return NVS_OK;
}

NVS_Error_t NVS_WriteUInt8(const char* namespace, const char *key, uint8_t value){
    nvs_handle_t nvs_handle;

    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_set_u8(nvs_handle, key, value);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Write Key %s/%s Fail, rsn=%s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_commit(nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Commit Fail, rsn=%s, %s(%d)", esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    nvs_close(nvs_handle);
    return NVS_OK;
}

NVS_Error_t NVS_WriteUInt16(const char* namespace, const char *key, uint16_t value){
    nvs_handle_t nvs_handle;

    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_set_u16(nvs_handle, key, value);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Write Key %s/%s Fail, rsn=%s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_commit(nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Commit Fail, rsn=%s, %s(%d)", esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    nvs_close(nvs_handle);
    return NVS_OK;
}

NVS_Error_t NVS_WriteUInt32(const char* namespace, const char *key, uint32_t value){
    nvs_handle_t nvs_handle;

    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_set_u32(nvs_handle, key, value);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Write Key %s/%s Fail, rsn=%s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_commit(nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Commit Fail, rsn=%s, %s(%d)", esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    nvs_close(nvs_handle);
    return NVS_OK;
}


NVS_Error_t NVS_WriteUInt64(const char* namespace, const char *key, uint64_t value){
    nvs_handle_t nvs_handle;

    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_set_u64(nvs_handle, key, value);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Write Key %s/%s Fail, rsn=%s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_commit(nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Commit Fail, rsn=%s, %s(%d)", esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    nvs_close(nvs_handle);
    return NVS_OK;
}

NVS_Error_t NVS_ReadUInt8(const char* namespace, const char *key, uint8_t *value){
    nvs_handle_t nvs_handle;
    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_get_u8(nvs_handle, key, value);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Read Key %s/%s Fail, rsn=%s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }
    nvs_close(nvs_handle);   
    return NVS_OK; 
}

NVS_Error_t NVS_ReadUInt16(const char* namespace, const char *key, uint16_t *value){
    nvs_handle_t nvs_handle;
    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_get_u16(nvs_handle, key, value);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Read Key %s/%s Fail, rsn=%s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }
    nvs_close(nvs_handle);   
    return NVS_OK; 
}

NVS_Error_t NVS_ReadUInt32(const char* namespace, const char *key, uint32_t *value){
    nvs_handle_t nvs_handle;
    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_get_u32(nvs_handle, key, value);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Read Key %s/%s Fail, rsn=%s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }
    nvs_close(nvs_handle);   
    return NVS_OK; 
}


NVS_Error_t NVS_ReadUInt64(const char* namespace, const char *key, uint64_t *value){
    nvs_handle_t nvs_handle;
    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_get_u64(nvs_handle, key,value);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Read Key %s/%s Fail, rsn=%s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    nvs_close(nvs_handle);
    return NVS_OK;
}


NVS_Error_t NVS_EraseKeyInNamespace(const char* namespace, const char *key){
    nvs_handle_t nvs_handle;
    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

        err = nvs_erase_key(nvs_handle,key);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Erase Key %s/%s Fail, rsn=%s, %s(%d)", namespace, key, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_commit(nvs_handle);

    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Commit Fail, rsn=%s, %s(%d)", esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    nvs_close(nvs_handle);
    return NVS_OK;
}

NVS_Error_t NVS_EraseAllKeysInNamespace(const char* namespace)
{
    nvs_handle_t nvs_handle;
    esp_err_t err = NVS_OpenNameSpace(namespace, &nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Open Namespace %s Fail, rsn = %s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }

    err = nvs_erase_all(nvs_handle);
    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Erase All Key in %s Fail, rsn=%s, %s(%d)", namespace, esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }
    err = nvs_commit(nvs_handle);

    if (err != ESP_OK)
    {
        NVS_LOGE(TAG, "Commit Fail, rsn=%s, %s(%d)", esp_err_to_name(err), __FUNCTION__, __LINE__);
        return NVS_FAIL;
    }
    nvs_close(nvs_handle);
    return NVS_OK;
}

NVS_Error_t NVS_EraseAll()
{
    for (NVS_Namspace_t* object = NVS_NamespaceHead; object != NULL; object = object->next)
    {
        if (NVS_EraseAllKeysInNamespace(object->name) == NVS_FAIL)
        {
            return NVS_FAIL;
        }
    }
    return NVS_OK;
}

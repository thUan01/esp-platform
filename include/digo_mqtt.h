#ifndef __DIGO_MQTT__
#define __DIGO_MQTT__

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>
#include <esp_log.h>


typedef enum {
    MQTTCLI_OK,
    MQTTCLI_FAIL,
}MQTTCLI_Error_t;

typedef enum {
    MQTTCLI_QOS0,
    MQTTCLI_QOS1,
    MQTTCLI_QOS2,
}MQTTCLI_QOS_t;

typedef enum 
{
    MQTTCLI_CONNECTED_EVT,
    MQTTCLI_DISCONNECTED_EVT,
    MQTTCLI_SUBSCRIBED_EVT,
    MQTTCLI_PUBLISHED_EVT,
    MQTTCLI_DATA_EVT
}MQTTCLI_CallbackEvent_t;

typedef union {
    struct mqttcli_subscribed
    {
        uint32_t msgID;
    }sub;

    struct mqttcli_published
    {
        uint32_t msgID;
    }pub;

    struct mqttcli_data 
    {
        char* topic;
        uint8_t topicLen;
        char* payload;
        uint32_t len;
    }data;
}MQTTCLI_CallbackParam_t;

typedef struct {
    const char* host;
    uint16_t port;
    const char* user;
    const char* password;
    const char* clientID;
    const char* certPem;
    bool isSSL;
}MQTTCLI_Info_t;

typedef void (*MQTT_Callback_t)(MQTTCLI_CallbackEvent_t event, MQTTCLI_CallbackParam_t* param);


/**
 * @brief MQTT Initialize
 * @param info  MQTT Client Information
 * @return MQTTCLI_OK when initialize MQTT success, MQTTCLI_FAIL if vice and versa
 */
MQTTCLI_Error_t MQTT_Init(MQTTCLI_Info_t info);


/**
 * @brief MQTT Start
 * @param none
 * @return MQTTCLI_OK when start MQTT success, MQTTCLI_FAIL if can't find client that has been initialized
 */
MQTTCLI_Error_t MQTT_Start(void);


/**
 * @brief MQTT Stop
 * @param none
 * @return MQTTCLI_OK when stop MQTT success, MQTTCLI_FAIL if vice and versa
 */
MQTTCLI_Error_t MQTT_Stop(void);


/**
 * @brief Register MQTT Callback
 * @param cb    callback function 
 */
void MQTT_RegisterCallback(MQTT_Callback_t cb);


/**
 * @brief Unregister MQTT Callback
 */
void MQTT_UnRegisterCallback();


/**
 * @brief MQTT Publish
 * @param topic         Publish Topic
 * @param data          Publish Data 
 * @param qos           Quality of Service MQTT
 * @param retain        MQTT Retain
 * @return MQTTCLI_OK when publish successfully, MQTTCLI_FAIL if vice and versa
 */
MQTTCLI_Error_t MQTT_Pub(const char* topic, const char* data, MQTTCLI_QOS_t qos, bool retain);


/**
 * @brief MQTT Publish
 * @param topic         Publish Topic
 * @param qos           Quality of Service MQTT
 * @return MQTTCLI_OK when subscribe successfully, MQTTCLI_FAIL if vice and versa
 */
MQTTCLI_Error_t MQTT_Subscribe(const char* topic, MQTTCLI_QOS_t qos);

#endif/*__DIGO_MQTT__*/
#ifndef __DIGO_SYSLOG_H__
#define __DIGO_SYSLOG_H__

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <esp_log.h>

#define CAUSE_BOOT_REASON       1
#define CAUSE_OTA_RESULT        7
#define CAUSE_WIFI_CONNECT      12
#define CAUSE_MQTT_CONNECT      13
#define CAUSE_TCP_CONNECT       14
#define CAUSE_SC_RESULT         15
#define CAUSE_APP_CONFIG        15
#define CAUSE_FACTORY_RST       16
#define CAUSE_REBOOT            17
#define CAUSE_HEAP_OVERFLOW     18
#define CAUSE_MQTT_DISCONNECT   19
#define CAUSE_SWITCH            20
#define CAUSE_CONNECT_CHANGE    21
#define CAUSE_SCAN_DONE_TIMEOUT 22

#define EXTRA_MBEDTLS           0
#define EXTRA_ESPTLS            1
#define EXTRA_SOCK              2

#define EXTRA_SMARTCF           0
#define EXTRA_HTTP_SERVER       1
#define EXTRA_STNP_SYNC         2
#define EXTRA_MQTT_CONNECT      3

#define EXTRA_SERVER            0
#define EXTRA_MANUAL_MODE       1
#define EXTRA_CONT_MODE         2
#define EXTRA_SCHEDULE          3
#define EXTRA_BLE               4
#define EXTRA_TEMP_PASS         5
#define EXTRA_TEST              6
#define EXTRA_MATTER            7
#define EXTRA_SENSOR_ERROR      8

#define VALUE_SUCCESS           0
#define VALUE_FAIL              1

#define VALUE_REBOOT_POWERON    0
#define VALUE_REBOOT_SOFT       1
#define VALUE_REBOOT_WDT        2 

#define VALUE_OFF               0
#define VALUE_ON                1

typedef enum {
    SYSLOG_OK,
    SYSLOG_FAIL
}SYSLOG_Error_t;

SYSLOG_Error_t SYSLOG_Init(void);
SYSLOG_Error_t SYSLOG_SaveOneRecord(uint8_t *data);
SYSLOG_Error_t SYSLOG_ReadBlockIndex(uint32_t blockIndex, uint8_t *data);
uint32_t SYSLOG_GetBlockIndex(void);
SYSLOG_Error_t SYSLOG_Save(uint8_t cause, uint16_t value, uint8_t extra);

#endif/*__DIGO_SYSLOG_H__*/
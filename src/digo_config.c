#include "digo_config.h"
#include "digo_nvs_store.h"

#include <esp_http_server.h>
#include <cJSON.h>
#include <inttypes.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>

#if (CONFIG_LOG_DIGO_CONFIG_ENABLED == true)

#define DIGO_CONFIG_LOGI(tag, fmt, ...) ESP_LOGI(tag, fmt, ##__VA_ARGS__)
#define DIGO_CONFIG_LOGE(tag, fmt, ...) ESP_LOGI(tag, fmt, ##__VA_ARGS__)
#define DIGO_CONFIG_LOGD(tag, fmt, ...) ESP_LOGI(tag, fmt, ##__VA_ARGS__)

#elif (CONFIG_LOG_DIGO_CONFIG_ENABLED == false)

#define DIGO_CONFIG_LOGI(tag, fmt, ...)
#define DIGO_CONFIG_LOGE(tag, fmt, ...)
#define DIGO_CONFIG_LOGD(tag, fmt, ...)
#endif /*CONFIG_LOG_DIGO_CONFIG_ENABLED*/

#define MIN(a,b) (((a)<(b))?(a):(b))

static httpd_handle_t MQTTTCFG_Server = NULL;
static const char* TAG = "MQTT CONFIG";

static uint32_t DIGOCFG_DeviceID = 0;
static char DIGOCFG_ProductID[32] = {0};
static uint16_t DIGOCFG_DeviceFirmware = 0;
static uint16_t DIGOCFG_DeviceHardware = 0;
static char DIGOCFG_MacAddress[32] = {0};
static char DIGO_TenantID[128] = {0};
static char DIGO_DeviceID[128] = {0};
static uint16_t DIGO_DeviceProfileID = 0;
static char DIGO_Token[128] = {0};
static DIGOCFG_Callback_t callback = NULL;
static TimerHandle_t SetupPostTimeoutTimerHandle = NULL;

static esp_err_t DIGOCFG_ServerPostHandler(httpd_req_t *request);
static esp_err_t DIGOCFG_ServerGetHandler(httpd_req_t *request);
static void SetupPostTimeoutHandler(TimerHandle_t xTimers);

static httpd_uri_t DIGOCFG_API_Setup = {
    .uri        = "/api/setup",
    .method     = HTTP_POST,
    .handler    = DIGOCFG_ServerPostHandler,
    .user_ctx   = NULL,
};

static httpd_uri_t DIGOCFG_API_Info = {
    .uri        = "/api/info",
    .method     = HTTP_GET,
    .handler    = DIGOCFG_ServerGetHandler,
    .user_ctx   = NULL,
};

DIGOCFG_Error_t DIGOCFG_Start(uint32_t id, const char* productID, uint16_t firmware, uint16_t hardware, const char* mac)
{
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;

    DIGOCFG_DeviceID = id;
    uint8_t len = sprintf(DIGOCFG_ProductID, "%s", productID); 
    DIGOCFG_ProductID[len] = '\0';
    len = sprintf(DIGOCFG_MacAddress, "%s", mac);
    DIGOCFG_MacAddress[len] = '\0';
    DIGOCFG_DeviceFirmware = firmware;
    DIGOCFG_DeviceHardware = hardware;

    DIGO_CONFIG_LOGI(TAG, "Star HTTP Server on Port [%"PRIu16"]", config.server_port);

    esp_err_t err = httpd_start(&MQTTTCFG_Server, &config);
    if (err == ESP_OK)
    {
        httpd_register_uri_handler(MQTTTCFG_Server, &DIGOCFG_API_Setup);
        httpd_register_uri_handler(MQTTTCFG_Server, &DIGOCFG_API_Info);
    }
    else
    {
        DIGO_CONFIG_LOGE(TAG, "Fail to Start HTTP Server on Port [%"PRIu16"], rsn = %s", config.server_port, esp_err_to_name(err));
        return DIGOCFG_FAIL;
    }
    SetupPostTimeoutTimerHandle = xTimerCreate("Setup Post Timeout",
                                                pdMS_TO_TICKS(CONFIG_SETUP_POST_EVENT_TIMEOUT),
                                                pdFALSE,
                                                NULL,
                                                SetupPostTimeoutHandler);
    if (SetupPostTimeoutTimerHandle != NULL)
    {
        xTimerStart(SetupPostTimeoutTimerHandle, 10);
    }
    else
    {
        DIGO_CONFIG_LOGE(TAG, "Fail to Create SetupPostTimeoutTimer");
    }
    return DIGOCFG_OK;
}

DIGOCFG_Error_t DIGOCFG_Stop(void)
{
    if (MQTTTCFG_Server == NULL)
    {
        DIGO_CONFIG_LOGE(TAG, "None of Active Server");
        return DIGOCFG_FAIL;
    }
    if (xTimerIsTimerActive(SetupPostTimeoutTimerHandle) == pdTRUE)
    {
        xTimerStop(SetupPostTimeoutTimerHandle, 0);
    }
    httpd_stop(MQTTTCFG_Server);
    return DIGOCFG_OK;
}

DIGOCFG_Error_t DIGOCFG_RegisterCallback(DIGOCFG_Callback_t cb)
{
    if (cb == NULL)
    {
        DIGO_CONFIG_LOGE(TAG, "Invalid Parameter");
        return DIGOCFG_FAIL;
    }
    callback = cb;
    return DIGOCFG_OK;

}


DIGOCFG_Error_t DIGOCFG_CheckInfoOnNVS(DIGOCFG_Info_t* info)
{
    if (NVS_ReadStr(CONFIG_MQTTCLI_NVS_NAMESPACE, CONFIG_MQTTCLI_NVS_KEY_HOST, info->host, sizeof(info->host), &info->hostLen) != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }

    if (NVS_ReadUInt16(CONFIG_MQTTCLI_NVS_NAMESPACE, CONFIG_MQTTCLI_NVS_KEY_PORT, &info->port) != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }
    if (NVS_ReadUInt8(CONFIG_MQTTCLI_NVS_NAMESPACE, CONFIG_MQTTCLI_NVS_KEY_SSL, &info->isSSLUsed) != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }
    #ifdef CONFIG_MQTT_USE_OLD_SERVER
    if (NVS_ReadStr(CONFIG_MQTTCLI_NVS_NAMESPACE, CONFIG_MQTTCLI_NVS_KEY_USER, info->user, sizeof(info->user), &info->userLen) != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }
    if (NVS_ReadStr(CONFIG_MQTTCLI_NVS_NAMESPACE, CONFIG_MQTTCLI_NVS_KEY_PWD, info->pwd, sizeof(info->pwd), &info->pwdLen) != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }

    #else/*CONFIG_MQTT_USE_OLD_SERVER*/
    if (NVS_ReadStr(CONFIG_VRT_DEV_NVS_NAMESPACE, CONFIG_VRT_DEV_NVS_KEY_TENANT_ID, info->tenantID, sizeof(info->tenantID), &info->tenantIDLen) != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }
    sprintf(DIGO_TenantID, "%s", info->tenantID);
    DIGO_TenantID[info->tenantIDLen] = '\0';

    if (NVS_ReadStr(CONFIG_VRT_DEV_NVS_NAMESPACE, CONFIG_VRT_DEV_NVS_KEY_DEVICE_ID, info->deviceID, sizeof(info->deviceID), &info->deviceIDLen) != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }
    sprintf(DIGO_DeviceID, "%s", info->deviceID);
    DIGO_DeviceID[info->deviceIDLen] = '\0';

    if (NVS_ReadUInt16(CONFIG_VRT_DEV_NVS_NAMESPACE, CONFIG_VRT_DEV_NVS_KEY_DEVICE_PROFILE_ID, &info->deviceProfileID) != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }
    DIGO_DeviceProfileID = info->deviceProfileID;

    if (NVS_ReadStr(CONFIG_VRT_DEV_NVS_NAMESPACE, CONFIG_VRT_DEV_NVS_KEY_TOKEN, info->token, sizeof(info->token), &info->tokenLen) != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }
    sprintf(DIGO_Token, "%s", info->token);
    DIGO_Token[info->tokenLen] = '\0';
    #endif /*CONFIG_MQTT_USE_OLD_SERVER*/
    return DIGOCFG_OK;
}

DIGOCFG_Error_t DIGOCFG_GetTenantID(char* tenantID)
{
    if (tenantID == NULL)
    {
        DIGO_CONFIG_LOGE(TAG, "Invalid Parameter");
        return DIGOCFG_FAIL;
    }
    uint8_t len = sprintf(tenantID, "%s", DIGO_TenantID);
    tenantID[len] = '\0';
    return DIGOCFG_OK;
}

DIGOCFG_Error_t DIGOCFG_GetDeviceID(char* deviceID)
{
    if (deviceID == NULL)
    {
        DIGO_CONFIG_LOGE(TAG, "Invalid Parameter");
        return DIGOCFG_FAIL;
    }
    uint8_t len = sprintf(deviceID, "%s", DIGO_DeviceID);
    deviceID[len] = '\0';
    return DIGOCFG_OK;
}

DIGOCFG_Error_t DIGOCFG_GetDeviceProfileID(uint16_t* profileID)
{
    *profileID = DIGO_DeviceProfileID;
    return DIGOCFG_OK;
}

DIGOCFG_Error_t DIGOCFG_GetToken(char* token)
{
    if (token == NULL)
    {
        DIGO_CONFIG_LOGE(TAG, "Invalid Parameter");
        return DIGOCFG_FAIL;
    }
    uint8_t len = sprintf(token, "%s", DIGO_Token);
    token[len] = '\0';
    return DIGOCFG_OK;
}

DIGOCFG_Error_t DIGOCFG_EraseNVSInfo(void)
{
    NVS_Error_t err = NVS_FAIL;
    err = NVS_EraseAllKeysInNamespace(CONFIG_MQTTCLI_NVS_NAMESPACE);
    if (err != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }
    err = NVS_EraseAllKeysInNamespace(CONFIG_VRT_DEV_NVS_NAMESPACE);
    if (err != NVS_OK)
    {
        return DIGOCFG_FAIL;
    }
    return DIGOCFG_OK;
}

static esp_err_t DIGOCFG_ServerGetHandler(httpd_req_t *request)
{
    cJSON* JSON_Response = cJSON_CreateObject();
    char* response = NULL;
    cJSON_AddNumberToObject(JSON_Response, "id", DIGOCFG_DeviceID);
    cJSON_AddNumberToObject(JSON_Response, "firmware", DIGOCFG_DeviceFirmware);
    cJSON_AddNumberToObject(JSON_Response, "hardware", DIGOCFG_DeviceHardware);
    cJSON_AddStringToObject(JSON_Response, "model", DIGOCFG_ProductID);
    cJSON_AddStringToObject(JSON_Response, "mac", DIGOCFG_MacAddress);

    response = cJSON_Print(JSON_Response);
    cJSON_Delete(JSON_Response);
    httpd_resp_send(request, response, HTTPD_RESP_USE_STRLEN);
    callback(DIGOCFG_API_InfoGetEvent, NULL);
    return ESP_OK;
}

static esp_err_t DIGOCFG_ServerPostHandler(httpd_req_t *request)
{
    uint32_t dataLen = request->content_len;
    uint32_t payloadRemain = 0;
    payloadRemain = dataLen;
    char buf[512] = {0};
    DIGO_CONFIG_LOGI(TAG, "Got Post on URI[%s]", request->uri);
    while (payloadRemain > 0)
    {
        uint32_t receivedLen = 0;
        if ((receivedLen = httpd_req_recv(request, buf, MIN(payloadRemain, sizeof(buf)))) <= 0)
        {
            if (receivedLen == HTTPD_SOCK_ERR_TIMEOUT)
            {
                continue;
            }
            return ESP_FAIL;
        }   
        payloadRemain -= receivedLen;
    }
    DIGO_CONFIG_LOGI(TAG, "Post Payload[%s]", buf);

    DIGOCFG_CallbackParam_t param;
    char responseOK[] = "{\"status\":true,\"error_code\":null}";
    char responseFail[] = "{\"status\":false,\"error_code\":null}";
    char* bufTemp = NULL;
    if (strcmp(request->uri, "api/setup"))
    {
        cJSON* root = cJSON_CreateObject();
        root = cJSON_Parse(buf);

        if (cJSON_GetObjectItem(root, "host"))
        {
            bufTemp = cJSON_GetObjectItem(root, "host")->valuestring;
            uint8_t len = sprintf(param.setupSuccess.host, "%s", bufTemp);
            param.setupSuccess.host[len] = '\0';
            NVS_WriteStr(CONFIG_MQTTCLI_NVS_NAMESPACE, CONFIG_MQTTCLI_NVS_KEY_HOST, param.setupSuccess.host);
        }
        else
        {
            goto SetupFail;
        }

        if (cJSON_GetObjectItem(root, "port"))
        {
            param.setupSuccess.port = cJSON_GetObjectItem(root, "port")->valueint;
            NVS_WriteUInt16(CONFIG_MQTTCLI_NVS_NAMESPACE, CONFIG_MQTTCLI_NVS_KEY_PORT, param.setupSuccess.port);
        }
        else
        {
            goto SetupFail;
        }
        if (cJSON_GetObjectItem(root, "ssl"))
        {
            param.setupSuccess.isSSLUsed = cJSON_GetObjectItem(root, "ssl")->valueint;
            NVS_WriteUInt8(CONFIG_MQTTCLI_NVS_NAMESPACE, CONFIG_MQTTCLI_NVS_KEY_SSL, param.setupSuccess.isSSLUsed);
        }   
    #ifdef CONFIG_MQTT_USE_OLD_SERVER
        if (cJSON_GetObjectItem(root, "user"))
        {
            bufTemp = cJSON_GetObjectItem(root, "user")->valuestring;
            uint8_t len = sprintf(param.setupSuccess.user, "%s", bufTemp);
            param.setupSuccess.user[len] = '\0';
            param.setupSuccess.userLen = len;
            NVS_WriteStr(CONFIG_MQTTCLI_NVS_NAMESPACE, CONFIG_MQTTCLI_NVS_KEY_USER, param.setupSuccess.user);
        }
        else
        {
            goto SetupFail;
        }

        if (cJSON_GetObjectItem(root, "pwd"))
        {
            bufTemp = cJSON_GetObjectItem(root, "pwd")->valuestring;
            uint8_t len = sprintf(param.setupSuccess.pwd, "%s", bufTemp);
            param.setupSuccess.pwd[len] = '\0';
            param.setupSuccess.pwdLen = len;
            NVS_WriteStr(CONFIG_MQTTCLI_NVS_NAMESPACE, CONFIG_MQTTCLI_NVS_KEY_PWD, param.setupSuccess.pwd);
        }
        else
        {
            goto SetupFail;
        }
    #else
        if (cJSON_GetObjectItem(root, "info"))
        {
            cJSON* info = cJSON_GetObjectItem(root, "info");
            if (cJSON_GetObjectItem(info, "tenant_id"))
            {
                bufTemp = cJSON_GetObjectItem(info, "tenant_id")->valuestring;
                uint8_t len = sprintf(param.setupSuccess.tenantID, "%s", bufTemp);
                param.setupSuccess.tenantID[len] = '\0';
                param.setupSuccess.tenantIDLen = len;
                NVS_WriteStr(CONFIG_VRT_DEV_NVS_NAMESPACE, CONFIG_VRT_DEV_NVS_KEY_TENANT_ID, param.setupSuccess.tenantID);
            }
            else
            {
                goto SetupFail;
            }
            if (cJSON_GetObjectItem(info, "device_id"))
            {
                bufTemp = cJSON_GetObjectItem(info, "device_id")->valuestring;
                uint8_t len = sprintf(param.setupSuccess.deviceID, "%s", bufTemp);
                param.setupSuccess.deviceID[len] = '\0';
                param.setupSuccess.deviceIDLen = len;
                NVS_WriteStr(CONFIG_VRT_DEV_NVS_NAMESPACE, CONFIG_VRT_DEV_NVS_KEY_DEVICE_ID, param.setupSuccess.deviceID);
            }
            else
            {
                goto SetupFail;
            }
            if (cJSON_GetObjectItem(info, "device_profile_id"))
            {
                param.setupSuccess.deviceProfileID = cJSON_GetObjectItem(info, "device_profile_id")->valueint;
                NVS_WriteUInt16(CONFIG_VRT_DEV_NVS_NAMESPACE, CONFIG_VRT_DEV_NVS_KEY_DEVICE_PROFILE_ID, param.setupSuccess.deviceProfileID);
            }
            else
            {
                goto SetupFail;
            }
            if (cJSON_GetObjectItem(info, "token"))
            {
                bufTemp = cJSON_GetObjectItem(info, "token")->valuestring;
                uint8_t len = sprintf(param.setupSuccess.token, "%s", bufTemp);
                param.setupSuccess.token[len] = '\0';
                param.setupSuccess.tokenLen = len;
                NVS_WriteStr(CONFIG_VRT_DEV_NVS_NAMESPACE, CONFIG_VRT_DEV_NVS_KEY_TOKEN, param.setupSuccess.token);
            }
            else
            {
                goto SetupFail;
            }
        
        }
        else
        {
            goto SetupFail;
        }
    #endif
        httpd_resp_send(request, responseOK, HTTPD_RESP_USE_STRLEN);
        //httpd_resp_send_chunk(request, NULL, 0);
        callback(DIGOCFG_API_SetupPostSuccessEvent, &param);
        return ESP_OK;
    }
    SetupFail:
        httpd_resp_send(request, responseFail, HTTPD_RESP_USE_STRLEN);
        //httpd_resp_send_chunk(request, NULL, 0);
        callback(DIGOCFG_API_SetupPostFailEvent, NULL);
        return ESP_OK;
}

static void SetupPostTimeoutHandler(TimerHandle_t xTimers)
{
    callback(DIGOCFG_API_SetupPostFailEvent, NULL);
}



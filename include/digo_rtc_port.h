#ifndef __RTC_PORT_H__
#define __RTC_PORT_H__

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

typedef enum {
    RTC_OK,
    RTC_FAIL,
}RTC_Error_t;

RTC_Error_t RTC_Init();
RTC_Error_t RTC_Reset();
RTC_Error_t RTC_SetTime(struct tm* time);
RTC_Error_t RTC_GetTime(struct tm* time);

#endif/*__RTC_PORT_H__*/